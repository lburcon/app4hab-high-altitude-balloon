package pl.edu.pwr.aerospace.app4hab.Tools

import android.content.IntentFilter
import org.mockito.ArgumentMatcher

/**
 * Created by t-rex on 06.02.2018.
 */
class IntentActionsMatcher(private val expectedActions: List<String>) : ArgumentMatcher<IntentFilter> {
    override fun matches(argument: IntentFilter?): Boolean {
        val actions = argument?.actionsIterator()?.asSequence()
        return actions?.all { x -> expectedActions.contains(x) } ?: false
    }
}