package pl.edu.pwr.aerospace.app4hab.APRSDroid

import android.content.Intent
import android.content.IntentFilter
import android.support.v7.app.AppCompatActivity
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.*
import org.robolectric.RobolectricTestRunner
import pl.edu.pwr.aerospace.app4hab.Tools.IntentActionsMatcher
import pl.edu.pwr.aerospace.app4hab.data.aprsdroid.APRSDroidEventHandler
import pl.edu.pwr.aerospace.app4hab.data.aprsdroid.APRSDroidEventReceiver

/**
 * Created by t-rex on 04.02.2018.
 */
@RunWith(RobolectricTestRunner::class)
class APRSDroidEventReceiverTest {

    private lateinit var activityMock : AppCompatActivity
    private lateinit var eventHandlerMock : APRSDroidEventHandler
    private lateinit var receiver: APRSDroidEventReceiver

    @Before
    fun setup() {
        activityMock = mock(AppCompatActivity::class.java)
        eventHandlerMock = mock(APRSDroidEventHandler::class.java)

        receiver = APRSDroidEventReceiver()
        receiver.eventHandler = eventHandlerMock
    }

    @Test
    fun registerReceiverAtRuntimeHasAllIntentsAndRegisterInContext() {
        val expectedActions = listOf(
                "org.aprsdroid.app.SERVICE_STARTED",
                "org.aprsdroid.app.SERVICE_STOPPED",
                "org.aprsdroid.app.MESSAGE",
                "org.aprsdroid.app.POSITION",
                "org.aprsdroid.app.UPDATE"
        )

        receiver.registerReceiver(activityMock)

        verify(activityMock).registerReceiver(eq(receiver), argThat(IntentActionsMatcher(expectedActions)))
        assertTrue(receiver.isRegistered)
    }

    @Test
    fun registerServiceCallsContext() {
        receiver.registerReceiver(activityMock)

        verify(activityMock).registerReceiver(eq(receiver), any(IntentFilter::class.java))
        assertTrue(receiver.isRegistered)
    }

    @Test
    fun unregisterServiceCallsContext() {
        receiver.registerReceiver(activityMock)

        assertTrue(receiver.isRegistered)

        receiver.unregisterReceiver(activityMock)

        verify(activityMock).unregisterReceiver(receiver)
        assertFalse(receiver.isRegistered)
    }

    @Test
    fun unregisterUnregisteredServiceNotCallContext() {
        receiver.unregisterReceiver(activityMock)

        assertFalse(receiver.isRegistered)
        verify(activityMock, never()).unregisterReceiver(receiver)
    }

    @Test
    fun onReceiveServiceStartedEvent() {
        val apiVersionValue = 1
        val callsignValue = "XXX"

        val intent = Intent("org.aprsdroid.app.SERVICE_STARTED")
        intent.putExtra("api_version", apiVersionValue)
        intent.putExtra("callsign", callsignValue)

        receiver.onReceive(activityMock, intent)

        verify(eventHandlerMock).onServiceStarted(apiVersionValue, callsignValue)
        assertTrue(receiver.aprsDroidRunning)
    }

    @Test
    fun onReceiveServiceStoppedEvent() {
        val intent = Intent("org.aprsdroid.app.SERVICE_STOPPED")

        receiver.onReceive(activityMock, intent)

        verify(eventHandlerMock).onServiceStopped()
        assertFalse(receiver.aprsDroidRunning)
    }

}