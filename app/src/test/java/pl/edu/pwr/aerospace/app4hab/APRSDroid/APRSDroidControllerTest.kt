package pl.edu.pwr.aerospace.app4hab.APRSDroid

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers
import org.mockito.Mockito.*
import org.robolectric.RobolectricTestRunner
import pl.edu.pwr.aerospace.app4hab.Tools.IntentExtrasMatcher
import pl.edu.pwr.aerospace.app4hab.Tools.IntentMatcher
import pl.edu.pwr.aerospace.app4hab.data.aprsdroid.APRSDroidBundle
import pl.edu.pwr.aerospace.app4hab.data.aprsdroid.APRSDroidController
import pl.edu.pwr.aerospace.app4hab.data.aprsdroid.APRSDroidControllerImpl

/**
 * Created by t-rex on 24.12.2017.
 */
@RunWith(RobolectricTestRunner::class)
class APRSDroidControllerTest {

    private lateinit var activityMock: AppCompatActivity
    private lateinit var APRSDroidController: APRSDroidController
    
    private val intentPackage = "org.aprsdroid.app"

    @Before
    fun setUp() {
        activityMock = mock(AppCompatActivity::class.java)
        APRSDroidController = APRSDroidControllerImpl(activityMock)
    }

    @Test
    fun runServiceOnceCalledProperly() {
        val action = "org.aprsdroid.app.ONCE"

        APRSDroidController.runServiceOnce()

        verify(activityMock).startService(ArgumentMatchers.any(Intent::class.java))
        verify(activityMock).startService(argThat(IntentMatcher(action, intentPackage)))
    }

    @Test
    fun runServiceCalledProperly() {
        val action = "org.aprsdroid.app.SERVICE"

        APRSDroidController.runService()

        verify(activityMock).startService(ArgumentMatchers.any(Intent::class.java))
        verify(activityMock).startService(argThat(IntentMatcher(action, intentPackage)))
    }

    @Test
    fun stopServiceCalledProperly() {
        val action = "org.aprsdroid.app.SERVICE_STOP"

        APRSDroidController.stopService()

        verify(activityMock).startService(ArgumentMatchers.any(Intent::class.java))
        verify(activityMock).startService(argThat(IntentMatcher(action, intentPackage)))
    }

    @Test
    fun sendPackageCalledProperly() {
        val action = "org.aprsdroid.app.SEND_PACKET"
        val bundle = APRSDroidBundle()

        APRSDroidController.sendPackage(bundle)

        verify(activityMock).startService(ArgumentMatchers.any(Intent::class.java))
        verify(activityMock).startService(argThat(IntentMatcher(action, intentPackage)))
    }

    @Test
    fun sendPackageHasAdditionalData() {
        val bundle = APRSDroidBundle()
        val intentDataExtra = "data"

        APRSDroidController.sendPackage(bundle)

        verify(activityMock).startService(argThat(IntentExtrasMatcher(listOf(intentDataExtra))))
    }
}