package pl.edu.pwr.aerospace.app4hab

import org.junit.Assert
import org.junit.Test
import pl.edu.pwr.aerospace.app4hab.data.atmega.atmegaMock.ConstantMessageProvider
import pl.edu.pwr.aerospace.app4hab.data.atmega.atmegaMock.VariableMessageProvider

/**
 * Created by Grzes on 04.02.2018.
 *
 * This tests are really manual tests, as some parts of message provider are random. For the test to
 * pass one has to manually check if message provider works as it should. This hurts my poor heart,
 * but oh well, it's just a mock
 */
class AtmegaMockTest {

    @Test
    fun CorrectNoOfDeliminatorsForConstantMP() {
        val messageProvider = ConstantMessageProvider()

        val message = messageProvider.message()
        val commas = countChars(message, ',')
        val dollars = countChars(message, '$')
        val asterics = countChars(message, '*')

        try {
            Assert.assertEquals(17, commas.toLong())
            Assert.assertEquals(1, asterics.toLong())
            Assert.assertEquals(1, dollars.toLong())
        } catch (e: AssertionError) {
            println(message)
            throw e
        }

    }

    @Test
    fun CorrectNoOfDeliminatorsForVariableMP() {
        val messageProvider = VariableMessageProvider()

        val message = messageProvider.message()
        val commas = countChars(message, ',')
        val dollars = countChars(message, '$')
        val asterics = countChars(message, '*')
        try {
            Assert.assertEquals(13, commas.toLong())
            Assert.assertEquals(1, asterics.toLong())
            Assert.assertEquals(1, dollars.toLong())
        } catch (e: AssertionError) {
            println(message)
            throw e
        }

    }

    @Test
    fun ConstantMessageProviderTest() {
        val messageProvider = ConstantMessageProvider()

        println("---===ConstantMessageProvider with no noise===---")
        for (i in 0..49) {
            System.out.println(messageProvider.message())
        }
        println("---=== end of ConstantMessageProvider with no noise===---")
        println()
        println()
    }

    @Test
    fun ConstantMessageProviderNoisedTest() {
        val messageProvider = ConstantMessageProvider()

        println("---===ConstantMessageProvider noised===---")
        for (i in 0..49) {
            println(messageProvider.messageNoised)
        }
        println("---=== end of ConstantMessageProvider noised===---")
        println()
        println()
    }

    @Test
    fun VariableMessageProviderTest() {
        val messageProvider = VariableMessageProvider()

        println("---===VariableMessageProvider with no noise===---")
        for (i in 0..49) {
            System.out.println(messageProvider.message())
        }
        println("---=== end of VariableMessageProvider with no noise===---")
        println()
        println()
    }

    @Test
    fun VariableMessageProviderNoisedTest() {
        val messageProvider = VariableMessageProvider()

        println("---===VariableMessageProvider noised===---")
        for (i in 0..49) {
            println(messageProvider.messageNoised)
        }
        println("---=== end of VariableMessageProvider noised===---")
        println()
        println()
    }


    private fun countChars(string: String, charToCount: Char): Int {
        var count = 0
        for (i in 0 until string.length) {
            if (string[i] == charToCount)
                count++
        }

        return count
    }
}
