package pl.edu.pwr.aerospace.app4hab.Tools

import android.content.Intent
import org.mockito.ArgumentMatcher
import java.util.HashMap

/**
 * Created by t-rex on 04.02.2018.
 */
class IntentExtrasMatcher(private val expectedExtras: List<String>) : ArgumentMatcher<Intent> {
    override fun matches(argument: Intent?): Boolean {
        val hasExtrasList = expectedExtras.map { x -> argument?.hasExtra(x) }
        return (!hasExtrasList.contains(false))
    }

}