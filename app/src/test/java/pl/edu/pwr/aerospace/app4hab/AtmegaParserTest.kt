package pl.edu.pwr.aerospace.app4hab

import org.junit.Test
import org.junit.Assert.*
import pl.edu.pwr.aerospace.app4hab.data.atmega.AtmegaParserImpl

class AtmegaParserImplTest {

    @Test
    fun parsingBytearrayWithCorrectData() {
        //GIVEN
        val data = "\$GOLEC,21,37,1.7,10.04,9,11.5,20.4,12.3,32.1,9,2.2,3.3,4.4*47"
        val parser : AtmegaParserImpl

        //WHEN
        parser = AtmegaParserImpl(data.toByteArray())
        val model = parser.getAtmegaModel()

        //THEN
        assertEquals(data, model.original)
        assertEquals(21, model.number)
        assertEquals(37, model.relativeTime)
        assertEquals(1.7f, model.latitude)
        assertEquals(10.04f, model.longitude)
        assertEquals(9, model.altitude)
        assertEquals(11.5f, model.out_temp)
        assertEquals(20.4f, model.inTemp)
        assertEquals(12.3f, model.pressure)
        assertEquals(32.1f, model.batteryVoltage)
        assertEquals(9, model.currentUsage)
        assertEquals(2.2f, model.accX)
        assertEquals(3.3f, model.accY)
        assertEquals(4.4f, model.accZ)
        assertEquals(71, model.checksum)
        assertTrue(model.isValid)
    }

    @Test
    fun parsingBytearrayOfRandomCharacters() {
        //GIVEN
        val data = "asd,z!xc,asd,^^,qe$,12das1,fh,fgZXh13,-200x,f1,12c,,"
        val parser : AtmegaParserImpl
        //WHEN
        parser = AtmegaParserImpl(data.toByteArray())
        val model = parser.getAtmegaModel()
        //THEN
        assertEquals(data, model.original)
        assertEquals(null, model.relativeTime)
        assertEquals(null, model.latitude)
        assertEquals(null, model.longitude)
        assertEquals(null, model.altitude)
        assertEquals(null, model.out_temp)
        assertEquals(null, model.inTemp)
        assertEquals(null, model.pressure)
        assertEquals(null, model.batteryVoltage)
        assertEquals(null, model.currentUsage)
        assertEquals(null, model.accX)
        assertEquals(null, model.accY)
        assertEquals(null, model.accZ)
        assertEquals(null, model.checksum)
        assertFalse(model.isValid)
    }

    @Test
    fun parsingBytearrayWithPartiallyWrongData() {
        //GIVEN
        val data = "\$GOLEC,21,2A,1.7,10.04,x,11.5,20.4,12.3,32.1,1.2x,2.2,3.3,4.4*47"
        val parser : AtmegaParserImpl

        //WHEN
        parser = AtmegaParserImpl(data.toByteArray())
        val model = parser.getAtmegaModel()

        //THEN
        assertEquals(data, model.original)
        assertEquals(21, model.number)
        assertEquals(null, model.relativeTime)
        assertEquals(1.7f, model.latitude)
        assertEquals(10.04f, model.longitude)
        assertEquals(null, model.altitude)
        assertEquals(11.5f, model.out_temp)
        assertEquals(20.4f, model.inTemp)
        assertEquals(12.3f, model.pressure)
        assertEquals(32.1f, model.batteryVoltage)
        assertEquals(null, model.currentUsage)
        assertEquals(2.2f, model.accX)
        assertEquals(3.3f, model.accY)
        assertEquals(4.4f, model.accZ)
        assertEquals(71, model.checksum)
        assertFalse(model.isValid)
    }

    @Test
    fun parsingEmptyBytearray() {
        //GIVEN
        val data = ""
        val parser : AtmegaParserImpl
        //WHEN
        parser = AtmegaParserImpl(data.toByteArray())
        val model = parser.getAtmegaModel()
        //THEN
        assertEquals(data, model.original)
        assertEquals(null, model.relativeTime)
        assertEquals(null, model.latitude)
        assertEquals(null, model.longitude)
        assertEquals(null, model.altitude)
        assertEquals(null, model.out_temp)
        assertEquals(null, model.inTemp)
        assertEquals(null, model.pressure)
        assertEquals(null, model.batteryVoltage)
        assertEquals(null, model.currentUsage)
        assertEquals(null, model.accX)
        assertEquals(null, model.accY)
        assertEquals(null, model.accZ)
        assertEquals(null, model.checksum)
        assertFalse(model.isValid)
    }

    @Test
    fun gettingAPRSDroidMessage(){
        //GIVEN
        val data = "\$GOLEC,21,37,1.7,10.04,9,11.5,20.4,12.3,32.1,9,2.2,3.3,4.4*47"
        val parser : AtmegaParserImpl

        //WHEN
        parser = AtmegaParserImpl(data.toByteArray())

        //THEN
        assertEquals("11.5,20.4,12.3,32.1,9,1.7,10.04,9", parser.getAPRSDroidMessage())
    }
}
