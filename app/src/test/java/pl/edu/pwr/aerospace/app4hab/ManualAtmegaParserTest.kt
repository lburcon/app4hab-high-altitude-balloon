package pl.edu.pwr.aerospace.app4hab

import org.junit.Test
import pl.edu.pwr.aerospace.app4hab.data.atmega.atmegaMock.VariableMessageProvider
import pl.edu.pwr.aerospace.app4hab.data.atmega.AtmegaParserImpl
import java.io.PrintWriter


/**
 * Created by Grzes on 07.02.2018.
 */
class ManualAtmegaParserTest {
    /**
     * Runs pseudomanual tests for atmega parser using atmega mock.
     *
     * This test runs in following manner:
     * - Atmega mock generates a message
     * - Parser tries to parse it
     * - If parser throws an exception test logs it to a file in classpath
     *
     * Above statements are repeated 100 000 times
     */
    @Test
    fun checkForExceptions() {
        val mp = VariableMessageProvider()
        val writer = PrintWriter("manualParserExceptionsTestResults.txt")
        var errors = 0

        for(i in 0..100000){
            if(i % 1000 == 0){
                System.out.println("Testing... " + (i / 1000f) +"%")
                System.out.print("" + errors + " errors so far")
                if(errors == 0)
                    System.out.print(" :)")
                else System.out.println()
            }

            val message = mp.messageNoised
            try {
                val parser = AtmegaParserImpl(message.toByteArray())
                val model = parser.getAtmegaModel()

            }catch (e: Exception){
                errors++
                logError(writer, message, e)
            }
        }

        writer.close()
    }

    @Test
    fun logResults(){
        val separator = " | "
        val mp = VariableMessageProvider()
        val writer = PrintWriter("parserResults.txt")

        for (i in 1..100){
            val message = mp.messageNoised
            val parser = AtmegaParserImpl(message.toByteArray())
            val model = parser.getAtmegaModel()
            writer.write("Generated: " + message.replace(",", separator).replace("*", separator) + "\n")
            writer.write("Parsed:             ")
            writer.write( "" + model.number + separator)
            writer.write( "" + model.relativeTime + separator)
            writer.write( "" + model.latitude + separator)
            writer.write( "" + model.longitude + separator)
            writer.write( "" + model.altitude + separator)
            writer.write( "" + model.out_temp + separator)
            writer.write( "" + model.inTemp + separator)
            writer.write( "" + model.pressure + separator)
            writer.write( "" + model.batteryVoltage + separator)
            writer.write( "" + model.currentUsage + separator)
            writer.write( "" + model.accX + separator)
            writer.write( "" + model.accY + separator)
            writer.write( "" + model.accZ + separator)
            writer.write( "" + model.checksum?.toString(16) + separator)
            writer.write("IsValid = " + model.isValid)
            writer.write("\n\n")
        }

        writer.close()
    }



    fun logError(writer: PrintWriter, message: String, e: Exception){
        writer.write("-----------=========================-----------\n")
        writer.write("Error while parsing line\n")
        writer.write("\t\t\t" + message + "\n")
        writer.write("Error message: " + e.message + "\n")
        e.printStackTrace(writer = writer)
        writer.write("-----------=========================-----------\n\n")
    }
}