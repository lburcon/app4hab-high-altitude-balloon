package pl.edu.pwr.aerospace.app4hab.Tools

import android.content.Intent
import org.mockito.ArgumentMatcher

/**
 * Created by t-rex on 04.02.2018.
 */
class IntentMatcher(private val action: String, private val intentPackage: String) : ArgumentMatcher<Intent> {

    override fun matches(argument: Intent?): Boolean {
        return argument?.action == action && argument.`package` == intentPackage
    }

}
