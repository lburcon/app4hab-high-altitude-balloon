package pl.edu.pwr.aerospace.app4hab.data.net.models

/**
 * Errors codes in App for communicating with server.
 *
 * @param code
 */
enum class App4HabHeadquartersErrorCode(private val code: Int) {
    WRONG_HTTP_STATUS_CODE(1000),
    REQUEST_BODY_NOT_VALID(1001),

    INTERNAL_RETROFIT_ERROR(2000),

    UNEXPECTED(5000)
}