package pl.edu.pwr.aerospace.app4hab.service

import android.content.Context
import android.telephony.TelephonyManager
import android.telephony.SignalStrength
import android.telephony.PhoneStateListener

/**
 * Class used for monitoring network's signal strength
 */
class MyPhoneStateListener(context: Context): PhoneStateListener() {
    companion object {
        const val minimalStrength = 12
        const val errorStrength = 3
    }

    /**
     * It must be intialised in order to be notified about network's states
     */
    var mStrengthListener: OnMinimalStrengthAchievedListener? = null
    private var mMinimalWasAchieved = false
    @Volatile private var mSignalStrength = 0

    interface OnMinimalStrengthAchievedListener {
        fun onAboveMinimal()
        fun onBelowMinimal()
    }

    private var mTelephonyManager: TelephonyManager = context.getSystemService(Context.TELEPHONY_SERVICE)
            as TelephonyManager


    /**
     * Registers listener
     */
    fun startListening() {
        mTelephonyManager.listen(this, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS)
    }

    /**
     * Unregisters listener
     */
    fun stopListening() {
        mTelephonyManager.listen(this, PhoneStateListener.LISTEN_NONE)
    }

    override fun onSignalStrengthsChanged(signalStrength: SignalStrength) {
        super.onSignalStrengthsChanged(signalStrength)
        mSignalStrength = signalStrength.gsmSignalStrength

        // If sth changed and was below/above level run proper listener
        if (mSignalStrength > minimalStrength && !mMinimalWasAchieved) {
            synchronized(this) {
                mStrengthListener?.onAboveMinimal()
                mMinimalWasAchieved = true
            }
        } else if (mSignalStrength < minimalStrength && mMinimalWasAchieved) {
            synchronized(this) {
                mStrengthListener?.onBelowMinimal()
                mMinimalWasAchieved = false
            }
        }
    }
}