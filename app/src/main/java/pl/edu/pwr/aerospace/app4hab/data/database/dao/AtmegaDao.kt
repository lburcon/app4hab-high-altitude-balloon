package pl.edu.pwr.aerospace.app4hab.data.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import pl.edu.pwr.aerospace.app4hab.data.database.models.AtmegaModel

/**
 * Created by lukasz.burcon on 19.12.2017.
 */

@Dao
interface AtmegaDao {

    @Query("SELECT * FROM atmega_data")
    fun getAllAtmegaData(): List<AtmegaModel>

    @Query("SELECT * FROM atmega_data WHERE id = (SELECT MAX(id) FROM atmega_data)")
    fun getLastMeasurement(): List<AtmegaModel>

    @Insert
    fun insertAtmegaModel(atmegaModel: AtmegaModel)
}