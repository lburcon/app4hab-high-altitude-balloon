package pl.edu.pwr.aerospace.app4hab.data.threads

import android.os.AsyncTask
import android.util.Log
import pl.edu.pwr.aerospace.app4hab.data.DataController
import pl.edu.pwr.aerospace.app4hab.data.collectors.device.DeviceSensorsDataCollector
import pl.edu.pwr.aerospace.app4hab.data.database.models.DeviceSensorsModel


/**
 * AsyncTask which reads data from Device, parses it and saves to the database
 * every specified time interval
 *
 */
class DeviceSensorsAsyncTask private constructor(private val mDataController: DataController,
                             private val mDeviceSensorsDataCollector: DeviceSensorsDataCollector) : AsyncTask<Void, Void, Void>() {
    companion object {
        /**
         * Interval time for each AsyncTask's execution
         */
        const val INTERVAL_TIME = 1000L
        const val DEBUG_TAG = "DeviceSensorsAsyncTask"
        private var instance: DeviceSensorsAsyncTask? = null

        fun stopInstance() {
            synchronized(this) {
                instance?.cancel(true)
                instance = null
            }
        }

        fun startInstance(dataController: DataController, deviceSensorsDataCollector: DeviceSensorsDataCollector) {
            synchronized(this) {
                if (instance != null) {
                    stopInstance()
                }
                instance = DeviceSensorsAsyncTask(dataController, deviceSensorsDataCollector)
                instance?.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
            }
        }
    }

    /**
     * Saves device's sensors model to the database
     */
    override fun doInBackground(vararg params: Void?): Void? {
        Log.d(DEBUG_TAG,"background")
        saveToDatabase(getDeviceSensorsModel())
        Thread.sleep(INTERVAL_TIME)
        return null
    }

    private fun getDeviceSensorsModel(): DeviceSensorsModel {
        return mDeviceSensorsDataCollector.getDeviceSensorsModel()
    }

    /**
     * Save AtmegaModel to database
     *
     * @param deviceSensorsModel model to save
     */
    private fun saveToDatabase(deviceSensorsModel: DeviceSensorsModel) {
        mDataController.addDeviceModelToDatabase(deviceSensorsModel)
        Log.d(DEBUG_TAG,"Device sensors data saved to Database")
    }

    /**
     * Start new instance of this class in order to start looping
     */
    override fun onPostExecute(result: Void?) {
        super.onPostExecute(result)
        Log.d(DEBUG_TAG,"onPost")
        synchronized(this) {
            instance = DeviceSensorsAsyncTask(mDataController, mDeviceSensorsDataCollector)
            instance?.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
        }
    }

    /**
     * Handle cancelling of this AsyncTask
     */
    override fun onCancelled() {
        Log.d(DEBUG_TAG,"onCancel")
        this.cancel(true)
        super.onCancelled()
    }
}