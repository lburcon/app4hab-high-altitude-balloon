package pl.edu.pwr.aerospace.app4hab.data.threads

import android.os.AsyncTask
import android.util.Log

/**
 * Used for connecting with external server
 * @param action action executed in the background
 */
class RetrofitAsyncTask(private val action: () -> Unit) : AsyncTask<Void, Void, Void>() {
    companion object {
        const val INTERVAL_TIME = 10000L
        const val DEBUG_TAG = "RetrofitAsyncTask"

        private var instance: RetrofitAsyncTask? = null

        fun stopInstance() {
            synchronized(this) {
                instance?.cancel(true)
                instance = null
            }
        }

        fun startInstance(action: () -> Unit) {
            synchronized(this) {
                if (instance != null) {
                    stopInstance()
                }
                instance = RetrofitAsyncTask(action)
                instance?.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
            }
        }
    }

    override fun doInBackground(vararg params: Void?): Void? {
        Log.d(DEBUG_TAG,"background")
        action.invoke()
        Thread.sleep(INTERVAL_TIME)
        return null
    }

    /**
     * Start new instance of this class in order to start looping
     */
    override fun onPostExecute(result: Void?) {
        super.onPostExecute(result)
        Log.d(DEBUG_TAG,"onPost")
        synchronized(this) {
            if (instance != null) {
                instance?.cancel(true)
            }
            instance = RetrofitAsyncTask(action)
            instance?.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
        }
    }

    /**
     * Handle cancelling of this AsyncTask
     */
    override fun onCancelled() {
        Log.d(DEBUG_TAG,"onCancel")
        this.cancel(true)
        super.onCancelled()
    }
}