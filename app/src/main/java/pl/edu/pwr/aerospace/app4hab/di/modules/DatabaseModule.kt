package pl.edu.pwr.aerospace.app4hab.di.modules

import android.arch.persistence.room.Room
import android.content.Context
import dagger.Module
import dagger.Provides
import pl.edu.pwr.aerospace.app4hab.R
import pl.edu.pwr.aerospace.app4hab.data.database.AppDatabase
import pl.edu.pwr.aerospace.app4hab.data.database.dao.AtmegaDao
import pl.edu.pwr.aerospace.app4hab.data.database.dao.DeviceDao

/**
 * Created by lukasz.burcon on 20.12.2017.
 */
@Module(includes = arrayOf(ApplicationModule::class))
class DatabaseModule {

    @Provides
    fun provideApplicationDatabase(context: Context): AppDatabase =
            Room.databaseBuilder(context, AppDatabase::class.java, context.getString(R.string.app4habDB))
                    // TODO VERY IMPORTANT, BE READY FOR UPDATING DATABASE. THIS CALL WILL REMOVE ALL DATA FROM DATABASE IF DB VERSION INCREASES.
                    .fallbackToDestructiveMigration()
                    .build()

    @Provides
    fun provideAtmegaDao(database: AppDatabase): AtmegaDao = database.atmegaDao()

    @Provides
    fun provideDeviceDao(database: AppDatabase): DeviceDao = database.deviceDao()
}