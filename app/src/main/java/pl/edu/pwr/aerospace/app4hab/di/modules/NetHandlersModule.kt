package pl.edu.pwr.aerospace.app4hab.di.modules

import android.content.Context
import dagger.Module
import dagger.Provides
import pl.edu.pwr.aerospace.app4hab.data.database.dao.AtmegaDao
import pl.edu.pwr.aerospace.app4hab.data.database.dao.DeviceDao
import pl.edu.pwr.aerospace.app4hab.data.net.App4HabHeadquartersService
import pl.edu.pwr.aerospace.app4hab.data.net.photoHandler.PhotoHandler
import pl.edu.pwr.aerospace.app4hab.data.net.photoHandler.PhotoHandlerImpl
import pl.edu.pwr.aerospace.app4hab.data.net.statusHandler.StatusHandler
import pl.edu.pwr.aerospace.app4hab.data.net.statusHandler.StatusHandlerImpl
import java.io.File
import javax.inject.Named

/**
 * Created by t-rex on 18/04/2018.
 */
@Module(includes = arrayOf(
        ApplicationModule::class,
        DatabaseModule::class,
        NetModule::class,
        FileModule::class))
class NetHandlersModule {

    @Provides
    fun providePhotoHandler(context: Context,
                             @Named("cameraDirectory") fileDirectory: File,
                             atmegaDao: AtmegaDao,
                             app4HabHeadquartersService: App4HabHeadquartersService) : PhotoHandler {
        return PhotoHandlerImpl(
                context,
                fileDirectory,
                atmegaDao,
                app4HabHeadquartersService
        )
    }

    @Provides
    fun provideStatusHandler(atmegaDao: AtmegaDao,
                             deviceDao: DeviceDao,
                             app4HabHeadquartersService: App4HabHeadquartersService) : StatusHandler {
        return StatusHandlerImpl(
                atmegaDao,
                deviceDao,
                app4HabHeadquartersService
        )
    }
}