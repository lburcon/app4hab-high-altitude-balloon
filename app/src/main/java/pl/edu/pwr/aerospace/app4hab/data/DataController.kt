package pl.edu.pwr.aerospace.app4hab.data

import android.hardware.SensorManager.getAltitude
import pl.edu.pwr.aerospace.app4hab.data.database.AppDatabase
import pl.edu.pwr.aerospace.app4hab.data.database.models.AtmegaModel
import pl.edu.pwr.aerospace.app4hab.data.database.models.DeviceSensorsModel

/**
 * Class used as database facade
 *
 */
class DataController(private val mDatabase: AppDatabase) {

    /**
     * Add AtmegaModel to database
     * @param model AtmegaModel
     */
    fun addAtmegaModelToDatabase(model: AtmegaModel) {
        mDatabase.atmegaDao().insertAtmegaModel(model)
    }

    /**
     * Adds device model to database
     */
    fun addDeviceModelToDatabase(model: DeviceSensorsModel) {
        mDatabase.deviceDao().insertDeviceSensorsModel(model)
    }

    /**
     * @return the last message saved in the database
     */
    fun getAPRSDroidMessage(): String {
        val model: AtmegaModel? = mDatabase.atmegaDao().getLastMeasurement().firstOrNull()

        return "${model?.out_temp}," +
                "${model?.inTemp}," +
                "${model?.pressure}," +
                "${model?.batteryVoltage}," +
                "${model?.currentUsage}," +
                "${model?.latitude}," +
                "${model?.longitude}," +
                "${model?.altitude}"
    }
}