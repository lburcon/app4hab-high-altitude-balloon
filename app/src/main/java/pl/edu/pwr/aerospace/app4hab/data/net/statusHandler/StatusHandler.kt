package pl.edu.pwr.aerospace.app4hab.data.net.statusHandler

/**
 * Class used to creating and sending statuses to external server
 */
interface StatusHandler {

    /**
     * Sends status to external server
     */
    fun sendStatus()
}