package pl.edu.pwr.aerospace.app4hab.data.atmega.atmegaMock

import java.util.*

/**
 * Provides messages used by APRSDroid
 */
abstract class MessageProvider {
    protected var rand = Random()

    /**
     * Generates an example message that atmega will send to phone
     * @return
     */
    abstract fun message(): String

    /**
     * Generates an example message that atmega will send to phone.
     *
     * Each message has 30% chance of being noised, to simulate possible errors during transmission
     * Noising is really replacing random characters (up to half of them) with other random characters
     * @return
     */
    val messageNoised: String
        get() {
            var message = message()
            if (rand.nextDouble() < 0.3) {
                val stringBuilder = StringBuilder(message)
                val randomInt = rand.nextInt(message.length / 2)
                for (i in 0 until randomInt)
                    stringBuilder.setCharAt(rand.nextInt(message.length), rand.nextInt(255).toChar())
                message = stringBuilder.toString()
            }

            return message
        }
}
