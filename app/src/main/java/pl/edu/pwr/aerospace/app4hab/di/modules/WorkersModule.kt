package pl.edu.pwr.aerospace.app4hab.di.modules

import android.content.Context
import dagger.Module
import dagger.Provides
import pl.edu.pwr.aerospace.app4hab.data.DataController
import pl.edu.pwr.aerospace.app4hab.data.aprsdroid.APRSDroidController
import pl.edu.pwr.aerospace.app4hab.data.aprsdroid.APRSDroidEventReceiver
import pl.edu.pwr.aerospace.app4hab.data.atmega.UsbReader
import pl.edu.pwr.aerospace.app4hab.data.camera.CameraCollector
import pl.edu.pwr.aerospace.app4hab.data.collectors.device.DeviceSensorsDataCollector
import pl.edu.pwr.aerospace.app4hab.workers.APRSDroidWorker
import pl.edu.pwr.aerospace.app4hab.workers.AtmegaWorker
import pl.edu.pwr.aerospace.app4hab.workers.CameraWorker
import pl.edu.pwr.aerospace.app4hab.workers.DeviceSensorsWorker

/**
 * Created by przemyslaw.materna on 30.03.2018.
 */
@Module(includes = arrayOf(ApplicationModule::class, ControllerModule::class, APRSDroidModule::class,
                            UsbReaderModule::class, CameraCollectorModule::class, DataCollectorsModule::class))
class WorkersModule {

    @Provides
    fun provideAtmegaWorker(usbReader: UsbReader): AtmegaWorker {
        return AtmegaWorker(usbReader)
    }

    @Provides
    fun providePhotoWorker(cameraCollector: CameraCollector, context: Context): CameraWorker {
        return CameraWorker(cameraCollector, context)
    }

    @Provides
    fun provideDeviceSensorsWorker(deviceSensorsDataCollector: DeviceSensorsDataCollector,
                                   dataController: DataController): DeviceSensorsWorker {
        return DeviceSensorsWorker(deviceSensorsDataCollector, dataController)
    }

    @Provides
    fun provideAPRSDroidWorker(aprsDroidReceiver: APRSDroidEventReceiver,
                               aprsDroidController: APRSDroidController,
                               dataController: DataController,
                               context: Context): APRSDroidWorker {
        return APRSDroidWorker(aprsDroidReceiver, aprsDroidController, dataController, context)
    }
}