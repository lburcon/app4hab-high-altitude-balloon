package pl.edu.pwr.aerospace.app4hab.data.aprsdroid

import android.location.Location
import android.util.Log


/**
 * Interface provides method to handle events form running APRSDroid Application
 */
interface APRSDroidEventHandler {
    fun onInternalError(message: String)
    fun onServiceStarted(apiVersion: Int, callsign: String)
    fun onServiceStopped()
    fun onMessage(source: String, dest: String, body: String)
    fun onPosition(source: String,
                   callsign: String,
                   location: Location)

    fun onUpdate(type: Int, status: String)
}

/**
 * 0: sent packet (generated by APRSdroid)
 * 1: info (APRSdroid's internal information message)
 * 2: error (something went wrong)
 * 3: incoming packet (received from APRS)
 *
 * @author Przemek Materna
 * @param code code of event
 */
enum class APRS_DROID_UPDATE_TYPE(private val code: Int) {
    SENT_PACKET(0),
    INFO(1),
    ERROR(2),
    INCOMING_PACKET(3);
}

/**
 * Class provides method to handle events form running APRSDroid Application
 * @author Przemek Materna
 * @see "https://aprsdroid.org/api/"
 */
class APRSDroidEventHandlerImpl : APRSDroidEventHandler {
    companion object {
        val DEBUG_TAG = "APRS Event"
    }

    /**
     * Called on internal receiver error (like wrong action name)
     */
    override fun onInternalError(message: String) {
        Log.d(DEBUG_TAG,message)
    }

    /**
     * @param apiVersion the API version code implemented by the installed version of APRSdroid
     * @param callsign  callsign and SSID of the APRSdroid user,
     *                  in the format "N0CALL-5" or "N0CALL" (if no SSID was configured)
     */
    override fun onServiceStarted(apiVersion: Int, callsign: String) {
        Log.d(DEBUG_TAG,"service started apiVersion:$apiVersion, callsign:$callsign")
    }

    override fun onServiceStopped() {
        Log.d(DEBUG_TAG,"service stopped")
    }

    /**
     * @param source callsign of the sender
     * @param dest callsign of the receiver
     * @param body content of the message
     */
    override fun onMessage(source: String, dest: String, body: String) {
        Log.d(DEBUG_TAG,"message from $source to:$dest: $body")
    }

    /**
     * @param source callsign that sent this packet
     * @param callsign callsign for which the position event was sent
     * @param location the geo-coordinates of the position
     */
    override fun onPosition(source: String, callsign: String, location: Location) {
        Log.d(DEBUG_TAG,"from $source with callsign: $callsign: $location")
    }

    /**
     * @param type the type of log entry
     * @param status the text that was added to the log
     *
     * @throws UnknownEventTypeException unknown type of event
     */
    override fun onUpdate(type: Int, status: String) {
        val eventType = matchEventCode(type)
        Log.d(DEBUG_TAG,"eventType: $eventType, position:$status")
    }


    /**
     * Matches event code with proper enum [APRS_DROID_UPDATE_TYPE].
     *
     * @throws UnknownEventTypeException non existing enum
     */
    private fun matchEventCode(type: Int): APRS_DROID_UPDATE_TYPE {
        return if (type in 0..3) APRS_DROID_UPDATE_TYPE.values()[type] else throw UnknownEventTypeException(type)
    }
}