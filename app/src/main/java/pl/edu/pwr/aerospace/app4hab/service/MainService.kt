package pl.edu.pwr.aerospace.app4hab.service

 import android.app.Activity
 import android.app.Service
 import android.content.Intent
 import android.os.Binder
 import android.os.IBinder
 import android.util.Log
 import pl.edu.pwr.aerospace.app4hab.MainApplication
 import pl.edu.pwr.aerospace.app4hab.data.net.App4HabHeadquartersService
 import pl.edu.pwr.aerospace.app4hab.di.components.ApplicationComponent
 import pl.edu.pwr.aerospace.app4hab.data.net.photoHandler.PhotoHandler
 import pl.edu.pwr.aerospace.app4hab.data.net.statusHandler.StatusHandler
 import pl.edu.pwr.aerospace.app4hab.workers.*

 import javax.inject.Inject
 import kotlin.reflect.KClass

/**
 * Service responsible for starting AsyncTasks with data collectors
 */
class MainService : Service() {
    companion object {
        private val DEBUG_TAG = "MainService"
    }

    inner class MainServiceLocalBinder : Binder() {
        fun getServiceInstance(): MainService {
            return this@MainService
        }
    }

    @Inject lateinit var mAPRSDroidWorker: APRSDroidWorker
    @Inject lateinit var mCameraWorker: CameraWorker
    @Inject lateinit var mDeviceSensorsWorker: DeviceSensorsWorker
    @Inject lateinit var mAtmegaWorker: AtmegaWorker
    @Inject lateinit var mRetrofitService: App4HabHeadquartersService
    @Inject lateinit var mStatusHandler: StatusHandler
    @Inject lateinit var mPhotoHandler: PhotoHandler

    private lateinit var mComponent: ApplicationComponent
    private lateinit var mainServiceController: MainServiceController
    private val localBinder = MainServiceLocalBinder()

    @Volatile private var mRemoteControlActive = false
        private set(value) {
            if (field != value) field = if (value) {
                mainServiceController.scheduleRemoteTask()
                value
            } else {
                mainServiceController.cancelRemoteTask()
                value
            }
        }

    fun setRemoteControlActive(active: Boolean, stateMap: Map<KClass<out Worker>, Boolean>?) {
        mRemoteControlActive = active
        if (stateMap != null) {
            this.changeWorkersState(stateMap)
        }
    }

    /**
     * Start AsyncTasks and initialize fields
     */
    override fun onCreate() {
        super.onCreate()
        init()
        Log.d(DEBUG_TAG,"created")

        mainServiceController = MainServiceController(
                this,
                MyPhoneStateListener(this),
                mRetrofitService,
                mPhotoHandler,
                mStatusHandler)
    }

    /**
     * Initialize fields
     */
    private fun init() {
        mComponent = (application as MainApplication).getApplicationComponent()
        mComponent.inject(this)
    }

    override fun onBind(intent: Intent?): IBinder? {
        return localBinder
    }

    /**
     * Start Service and determine it's behaviour after being killer
     */
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)
        return Service.START_NOT_STICKY //TODO: change to STICKY when app is done
    }

    fun <T: Worker> changeWorkersState(stateMap: Map<KClass<out T>, Boolean>) {
        stateMap.forEach { entry ->
            val workerState = getWorkerState(entry.key)

            // If worker is stopped or broken AND stateMap[worker] is true, then turn on.
            if ((workerState == WorkerState.BROKEN || workerState == WorkerState.STOPPED) && entry.value) {
                turnOnWorker(entry.key)
            }
            // Else if worker is running AND stateMap[worker] is false, then turn off
            else if (workerState == WorkerState.RUNNING && !entry.value) {
                turnOffWorker(entry.key)
            }
        }
    }

    private fun <T: Worker> turnOffWorker(clazz: KClass<T>) {
        getWorkerByType(clazz)?.stopWorker()
    }

    private fun <T: Worker> turnOnWorker(clazz: KClass<T>) {
        getWorkerByType(clazz)?.startWorker()
    }

    private fun <T: Worker> getWorkerState(clazz: KClass<T>): WorkerState? {
        return getWorkerByType(clazz)?.getState()
    }

    /**
     * Prints workers' state to logcat on debug level
     */
    fun printWorkersState() {
        getWorkers().forEach {
            Log.d(DEBUG_TAG,"${it.getWorkerName()}: ${it.getState()}")
        }
    }

    @Suppress("UNCHECKED_CAST")
    private fun <T: Worker> getWorkerByType(clazz: KClass<T>): T? {
        return getWorkers().find { worker ->
            (worker.getWorkerName() == clazz.getWorkerName())
        } as? T
    }

    fun getWorkers(): List<Worker> {
        return listOf(
                mAPRSDroidWorker,
                mDeviceSensorsWorker,
                mCameraWorker,
                mAtmegaWorker
        )
    }

}
