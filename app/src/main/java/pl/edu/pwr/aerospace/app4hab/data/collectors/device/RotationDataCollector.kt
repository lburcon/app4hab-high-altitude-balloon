package pl.edu.pwr.aerospace.app4hab.data.collectors.device

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import pl.edu.pwr.aerospace.app4hab.data.database.models.DeviceSensorsModel

/**
 * Class responsible for reading rotation data from phone [Sensor] and writing it to [DeviceSensorsModel]
 * Class implements [SensorEventListener] and [DataCollector] interface
 * @param context main application context
 */
class RotationDataCollector(context: Context) : SensorEventListener, DataCollector {
    /**
     * Description below assumes that the bottom edge of the device faces the user and that the screen is face-up
     * [pitch] angle of rotation about the X axis
     * [roll] angle of rotation about the Y axis
     * [azimuth] angle of rotation about the Z axis
     */
    private var azimuth: Double? = null
    private var pitch: Double? = null
    private var roll: Double? = null
    private var gravityArray = FloatArray(3)
    private var magneticArray = FloatArray(3)
    private val sensorManager: SensorManager by lazy {
        context.getSystemService(Context.SENSOR_SERVICE) as SensorManager
    }


    override fun onAccuracyChanged(p0: Sensor?, p1: Int) {
    }

    /**
     * Uses method described here https://developer.android.com/reference/android/hardware/SensorManager.html#getRotationMatrix(float[],%20float[],%20float[],%20float[])
     */
    override fun onSensorChanged(event: SensorEvent?) {
        if (event?.sensor?.type == Sensor.TYPE_ACCELEROMETER)
            gravityArray = event.values
        if (event?.sensor?.type == Sensor.TYPE_MAGNETIC_FIELD)
            magneticArray = event.values
        val R = FloatArray(9)
        val I = FloatArray(9)
        val success = SensorManager.getRotationMatrix(R, I, gravityArray, magneticArray)
        if (success) {
            val orientation = FloatArray(3)
            SensorManager.getOrientation(R, orientation)
            azimuth = Math.toDegrees(orientation[0].toDouble())
            pitch = Math.toDegrees(orientation[1].toDouble())
            roll = Math.toDegrees(orientation[2].toDouble())
        }
    }

    override fun registerCollector() {
        sensorManager.registerListener(
                this,
                sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),
                SensorManager.SENSOR_DELAY_NORMAL
        )
        sensorManager.registerListener(
                this,
                sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL
        )
    }

    /**
     * Writes rotation data to main phone sensors model
     */
    override fun getData(model: DeviceSensorsModel) {
        model.rotationX = pitch
        model.rotationY = roll
        model.rotationZ = azimuth
    }

    override fun unregisterCollector() {
        sensorManager.unregisterListener(this)
    }
}