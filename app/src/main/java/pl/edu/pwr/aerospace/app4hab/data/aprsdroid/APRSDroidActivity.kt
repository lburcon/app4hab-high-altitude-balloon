package pl.edu.pwr.aerospace.app4hab.data.aprsdroid

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_aprsdroid.*
import org.jetbrains.anko.toast
import pl.edu.pwr.aerospace.app4hab.R

/**
 * Class only for test properties.
 * Register [APRSDroidEventReceiver] at runtime
 * Gives possibilities to use actions from [APRSDroidController]
 */
class APRSDroidActivity : AppCompatActivity() {

    private var aprsDroidController: APRSDroidController = APRSDroidControllerImpl(this)
    private var receiver: APRSDroidEventReceiver = APRSDroidEventReceiver()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_aprsdroid)

        this.runOnceButton.setOnClickListener {
            aprsDroidController.runServiceOnce()
        }

        this.runButton.setOnClickListener {
            aprsDroidController.runService()
        }

        this.stopButton.setOnClickListener {
            aprsDroidController.stopService()
        }

        this.sendPackageButton.setOnClickListener {
            val aprsDroidBundle = if (this.sendPackageInput.text.isEmpty()) {
                APRSDroidBundle(this.packageName)
            } else {
                APRSDroidBundle(this.sendPackageInput.text.toString())
            }

            aprsDroidController.sendPackage(aprsDroidBundle)
            toast("Package send: $aprsDroidBundle")
        }

        // register receiver
        receiver.registerReceiver(applicationContext)
    }

    override fun onDestroy() {
        super.onDestroy()
        receiver.unregisterReceiver(applicationContext)
    }


}
