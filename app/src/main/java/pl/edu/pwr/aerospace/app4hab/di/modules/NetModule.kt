package pl.edu.pwr.aerospace.app4hab.di.modules

import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.Retrofit
import com.google.gson.Gson
import javax.inject.Singleton
import dagger.Provides
import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import android.app.Application
import android.util.Log
import com.facebook.stetho.okhttp3.StethoInterceptor
import dagger.Module
import okhttp3.Cache
import okhttp3.OkHttpClient
import pl.edu.pwr.aerospace.app4hab.SettingsModel
import pl.edu.pwr.aerospace.app4hab.data.net.App4HabHeadquarters
import pl.edu.pwr.aerospace.app4hab.data.net.App4HabHeadquartersService
import pl.edu.pwr.aerospace.app4hab.data.net.App4HabHeadquartersServiceImpl


/**
 * Dagger module for Network Connection
 *
 * @author Bartek Jakubczak
 */
@Module(includes = arrayOf(ApplicationModule::class))
class NetModule(private val settings: SettingsModel) {
    init {
        Log.d("Settings", "$settings")
    }

    @Provides
    @Singleton
    internal fun provideHttpCache(application: Application): Cache {
        val cacheSize = 10 * 1024 * 1024
        return Cache(application.cacheDir, cacheSize.toLong())
    }

    @Provides
    @Singleton
    internal fun provideGson(): Gson {
        return GsonBuilder().apply {
            setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        }.create()
    }

    @Provides
    @Singleton
    internal fun provideOkhttpClient(cache: Cache): OkHttpClient {
        return OkHttpClient.Builder().apply {
            cache(cache)
            addInterceptor {
                val request = it.request().newBuilder().addHeader(
                        settings.apiKeyName,
                        settings.apiKeyValue).build()
                it.proceed(request)
            }
            addNetworkInterceptor(StethoInterceptor())
        }.build()
    }

    @Provides
    @Singleton
    internal fun provideRetrofit(gson: Gson, okHttpClient: OkHttpClient): App4HabHeadquarters {
        return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(settings.serverAddress)
                .client(okHttpClient)
                .build()
                .create(App4HabHeadquarters::class.java)
    }

    @Provides
    @Singleton
    internal fun provideApp4HabHeadquartersService(retrofit: App4HabHeadquarters) : App4HabHeadquartersService {
        return App4HabHeadquartersServiceImpl(retrofit)
    }
}