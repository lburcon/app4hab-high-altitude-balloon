package pl.edu.pwr.aerospace.app4hab.data.net.models

/**
 * Exception for communicating with server.
 *
 * @param errorCode
 * @param msg message
 * @param cause cause of exception (ex: Retrofit exception...)
 */
class App4HabHeadquartesException(val errorCode: App4HabHeadquartersErrorCode = App4HabHeadquartersErrorCode.UNEXPECTED,
                                  msg: String? = null,
                                  cause: Throwable? = null) : Exception(msg, cause)