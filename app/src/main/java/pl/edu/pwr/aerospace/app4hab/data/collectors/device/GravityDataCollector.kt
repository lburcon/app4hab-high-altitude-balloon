package pl.edu.pwr.aerospace.app4hab.data.collectors.device

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import pl.edu.pwr.aerospace.app4hab.data.database.models.DeviceSensorsModel

/**
 * Class responsible for collecting gravity data in 3 axes: X, Y, Z
 * Class implements [SensorEventListener] and [DataCollector] interface
 * @param context main application context
 */
class GravityDataCollector(context: Context) : SensorEventListener, DataCollector {

    private var gravityX: Float? = null
    private var gravityY: Float? = null
    private var gravityZ: Float? = null
    private val sensorManager: SensorManager by lazy {
        context.getSystemService(Context.SENSOR_SERVICE) as SensorManager
    }

    override fun onAccuracyChanged(p0: Sensor?, p1: Int) {
    }

    override fun onSensorChanged(event: SensorEvent?) {
        gravityX = event?.values?.get(0)
        gravityY = event?.values?.get(1)
        gravityZ = event?.values?.get(2)
    }

    override fun registerCollector() {
        sensorManager.registerListener(
                this,
                sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY),
                SensorManager.SENSOR_DELAY_NORMAL
        )
    }

    /**
     * Writes gravity data to the main phone sensors model
     */
    override fun getData(model: DeviceSensorsModel) {
        model.gravityX = gravityX
        model.gravityY = gravityY
        model.gravityZ = gravityZ
    }

    override fun unregisterCollector() {
        sensorManager.unregisterListener(this)
    }

}