@file:Suppress("DEPRECATION")

package pl.edu.pwr.aerospace.app4hab.data.camera

import android.hardware.Camera

/**
 * Camera Configurator interface.
 * It's used for changing photo parameters in [CameraCollectorImpl]
 */
interface CameraConfigurator {
    fun configureCamera(params: Camera.Parameters): Camera.Parameters
}