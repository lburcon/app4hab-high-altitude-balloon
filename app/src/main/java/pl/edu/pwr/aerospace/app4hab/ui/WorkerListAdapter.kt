package pl.edu.pwr.aerospace.app4hab.ui

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Switch
import org.jetbrains.anko.find
import pl.edu.pwr.aerospace.app4hab.R
import pl.edu.pwr.aerospace.app4hab.workers.Worker

/**
 * Created by t-rex on 31/05/2018.
 */
class WorkerListAdapter(private val workerMap: List<WorkerSwitchModel>) : RecyclerView.Adapter<WorkerListAdapter.SingleWorkerView>() {
    init {
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): SingleWorkerView {
        return SingleWorkerView(LayoutInflater.from(parent?.context).inflate(R.layout.view_single_switch, parent, false))
    }

    override fun getItemCount(): Int {
        return workerMap.count()
    }

    override fun onBindViewHolder(holder: SingleWorkerView?, position: Int) {
        holder?.bind(workerMap.toList()[position])
    }

    class SingleWorkerView(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val switch = itemView.find<Switch>(R.id.single_switch)

        fun bind(model: WorkerSwitchModel) {
            switch.text = model.getDisplayedName()
            switch.setOnCheckedChangeListener { _, isChecked ->
                model.action.invoke(model.worker, isChecked)
            }
        }
    }

    data class WorkerSwitchModel(val worker: Worker,
                                 val qualifiedName: String,
                                 val action: (Worker, Boolean) -> Unit) {
        fun getDisplayedName(): String = "$qualifiedName: ${worker.getState()}"
    }
}

