package pl.edu.pwr.aerospace.app4hab.workers


/**
 * Defines worker's state
 */
enum class WorkerState {
    /**
     * Worker is stopped manually or didn't start
     */
    STOPPED,
    /**
     * Worker is lunching (means temporary state between STOPPED and LAUNCHING)
     */
    LAUNCHING, 
    /**
     * Worker is running
     */
    RUNNING, 
    /**
     * Worker is stopping (RUNNING - STOPPING - STOPPED)
     */
    STOPPING,  
    /**
     * Worker is broken (when throws exception, didn't lunch etc.)
     */
    BROKEN  
}