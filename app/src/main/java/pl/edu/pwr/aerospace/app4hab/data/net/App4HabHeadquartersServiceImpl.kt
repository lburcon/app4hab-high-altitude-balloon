package pl.edu.pwr.aerospace.app4hab.data.net

import pl.edu.pwr.aerospace.app4hab.data.net.models.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Base implementation of server service
 *
 * @see <a href = "https://docs.google.com/document/d/1SDoIV1kTva0E3iuzK7ytnPAANlwxzgzbm06HYPAtHfg/edit#heading=h.rv33mmm6nvd"</a>
 */
class App4HabHeadquartersServiceImpl(private val service: App4HabHeadquarters): App4HabHeadquartersService {

    /**
     * Handling: api/commands
     *
     * @param callback
     */
    override fun getCommands(callback: App4HabHeadquartersCallback<CommandModel>) {
        service.getCommands().enqueue(object: Callback<CommandModel> {
            override fun onResponse(call: Call<CommandModel>?, response: Response<CommandModel>?) {
                when {
                    response?.code() == 200 && response.body() != null -> callback.onSuccess(response.body()!!)
                    response?.code() != 204 -> callback.onFail(App4HabHeadquartesException(App4HabHeadquartersErrorCode.WRONG_HTTP_STATUS_CODE))
                    response.body() == null -> callback.onFail(App4HabHeadquartesException(App4HabHeadquartersErrorCode.REQUEST_BODY_NOT_VALID))
                    else -> callback.onFail(App4HabHeadquartesException(App4HabHeadquartersErrorCode.UNEXPECTED))
                }
            }

            override fun onFailure(call: Call<CommandModel>?, t: Throwable?) {
                callback.onFail(App4HabHeadquartesException(App4HabHeadquartersErrorCode.INTERNAL_RETROFIT_ERROR, null, t))
            }
        })
    }

    /**
     * Handling: api/status
     *
     * @param status
     * @param callback
     */
    override fun sendStatus(status: StatusModel, callback: App4HabHeadquartersCallback<App4HabHeadquartersResponseModel>) {
        service.sendStatus(status).enqueue(object : Callback<Void>{
            override fun onResponse(call: Call<Void>?, response: Response<Void>?) {
                when {
                    response?.code() == 204 -> callback.onSuccess(App4HabHeadquartersResponseModel())
                    response?.code() != 204 -> callback.onFail(App4HabHeadquartesException(App4HabHeadquartersErrorCode.WRONG_HTTP_STATUS_CODE))
                }
            }

            override fun onFailure(call: Call<Void>?, t: Throwable?) {
                callback.onFail(App4HabHeadquartesException(App4HabHeadquartersErrorCode.INTERNAL_RETROFIT_ERROR, null, t))
            }
        })
    }

    /**
     * Handling: api/photo
     *
     * @param photo
     * @param callback
     */
    override fun sendPhoto(photo: PhotoModel, callback: App4HabHeadquartersCallback<App4HabHeadquartersResponseModel>) {
        service.sendPhoto(photo).enqueue(object : Callback<Void>{
            override fun onResponse(call: Call<Void>?, response: Response<Void>?) {
                when{
                    response?.code() == 204 -> callback.onSuccess(App4HabHeadquartersResponseModel())
                    response?.code() != 204 -> callback.onFail(App4HabHeadquartesException(App4HabHeadquartersErrorCode.WRONG_HTTP_STATUS_CODE))
                }
            }
            override fun onFailure(call: Call<Void>?, t: Throwable?) {
                callback.onFail(App4HabHeadquartesException(App4HabHeadquartersErrorCode.INTERNAL_RETROFIT_ERROR, null, t))
            }
        })
    }
}