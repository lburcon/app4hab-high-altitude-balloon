package pl.edu.pwr.aerospace.app4hab.data.collectors.device

import android.app.ActivityManager
import android.content.Context
import pl.edu.pwr.aerospace.app4hab.data.database.models.DeviceSensorsModel

/**
 * Class responsible for collecting memory usage data
 * Class implements [DataCollector] interface
 * @param context main application context
 */
class MemoryUsageCollector(context: Context) : DataCollector {
    /**
     * This data collector does not require any SensorEventListener to be registered
     */
    override fun registerCollector() { }
    override fun unregisterCollector() { }

    private val activityManager: ActivityManager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
    private val memoryInfo = ActivityManager.MemoryInfo()

    /**
     * Function returns memory in use in MB as a difference between total memory and memory that is currently available
     */
    private fun getMemoryUsage(): Long {
        activityManager.getMemoryInfo(memoryInfo)
        return (memoryInfo.totalMem - memoryInfo.availMem) / 0x100000L
    }

    /**
     * Writes memory usage data to main phone sensors model
     */
    override fun getData(model: DeviceSensorsModel) {
        model.memoryUsage = getMemoryUsage()
    }

}