package pl.edu.pwr.aerospace.app4hab.di.modules

import android.os.Environment
import dagger.Module
import dagger.Provides
import java.io.File
import javax.inject.Named

/**
 * Created by t-rex on 18/04/2018.
 */
@Module
class FileModule {
    @Provides
    @Named("cameraDirectory")
    fun provideExternalStorage(): File {
        return File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "App4Hab")
    }
}