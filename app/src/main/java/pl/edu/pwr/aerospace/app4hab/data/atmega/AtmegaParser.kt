package pl.edu.pwr.aerospace.app4hab.data.atmega

import android.util.Log
import pl.edu.pwr.aerospace.app4hab.data.database.models.AtmegaModel

/**
 * Created by Grzes on 30.12.2017.
 */
interface AtmegaParser {
    fun getAtmegaModel(): AtmegaModel
    fun getAPRSDroidMessage(): String
}

class AtmegaParserImpl(input: ByteArray, private val currentTimestamp: Long = System.currentTimeMillis()) : AtmegaParser {
    companion object {
        const val NUMBER_INDEX = 1
        const val RELATIVE_TIME_INDEX = 2
        const val LATITUDE_INDEX = 3
        const val LONGITUDE_INDEX = 4
        const val ALTITUDE_INDEX = 5
        const val IN_TEMPERATURE_INDEX = 6
        const val OUT_TEMPERATURE_INDEX = 7
        const val PRESSURE_INDEX = 8
        const val BATTERY_VOLTAGE_INDEX = 9
        const val CURRENT_USAGE_INDEX = 10
        const val ACCELERATION_X_INDEX = 11
        const val ACCELERATION_Y_INDEX = 12
        const val ACCELERATION_Z_INDEX = 13
        const val UV_LIGHT_INTENSITY = 14
        const val ILLUMINANCE = 15
        const val CHECKSUM_INDEX = 16
    }

    private val originalInput = String(input)
    private val inputAsString = String(input)
    private val splittedData = inputAsString.split(",", "*")

    override fun getAtmegaModel(): AtmegaModel {
        return AtmegaModel(
                original = originalInput,
                absoluteTime = currentTimestamp,
                number = getNumber(),
                relativeTime = getRelativeTime(),
                out_temp = getOutTemp(),
                inTemp = getInTemp(),
                pressure = getPressure(),
                batteryVoltage = getBatteryVoltage(),
                currentUsage = getCurrentUsage(),
                accX = getAccX(),
                accY = getAccY(),
                accZ = getAccZ(),
                longitude = getLongitude(),
                latitude = getLatitude(),
                altitude = getAltitude(),
                uvLightIntensity = getUVLightIntensity(),
                illuminance = getIlluminance(),
                checksum = getChecksum(),
                isValid = isValid()
        )
    }

    override fun getAPRSDroidMessage(): String {
        return "${getOutTemp() ?: ""}," +
                "${getInTemp() ?: ""}," +
                "${getPressure() ?: ""}," +
                "${getBatteryVoltage() ?: ""}," +
                "${getCurrentUsage() ?: ""}," +
                "${getLatitude() ?: ""}," +
                "${getLongitude() ?: ""}," +
                "${getAltitude() ?: ""}"
    }

    private fun getNumber(): Int? {
        return getValueOrNull { splittedData[NUMBER_INDEX].toInt() }
    }

    private fun getRelativeTime(): Int? {
        return getValueOrNull { splittedData[RELATIVE_TIME_INDEX].toInt() }
    }

    private fun getLatitude(): Float? {
        return getValueOrNull { splittedData[LATITUDE_INDEX].toFloat() }
    }

    private fun getLongitude(): Float? {
        return getValueOrNull { splittedData[LONGITUDE_INDEX].toFloat() }
    }

    private fun getAltitude(): Int? {
        return getValueOrNull { splittedData[ALTITUDE_INDEX].toInt() }
    }

    private fun getOutTemp(): Float? {
        return getValueOrNull { splittedData[OUT_TEMPERATURE_INDEX].toFloat() }
    }

    private fun getInTemp(): Float? {
        return getValueOrNull { splittedData[IN_TEMPERATURE_INDEX].toFloat() }
    }

    private fun getPressure(): Float? {
        return getValueOrNull { splittedData[PRESSURE_INDEX].toFloat() }
    }

    private fun getBatteryVoltage(): Float? {
        return getValueOrNull { splittedData[BATTERY_VOLTAGE_INDEX].toFloat() }
    }

    private fun getCurrentUsage(): Int? {
        return getValueOrNull { splittedData[CURRENT_USAGE_INDEX].toInt() }
    }

    private fun getAccX(): Float? {
        return getValueOrNull { splittedData[ACCELERATION_X_INDEX].toFloat() }
    }

    private fun getAccY(): Float? {
        return getValueOrNull { splittedData[ACCELERATION_Y_INDEX].toFloat() }
    }

    private fun getAccZ(): Float? {
        return getValueOrNull { splittedData[ACCELERATION_Z_INDEX].toFloat() }
    }

    private fun getUVLightIntensity(): Int? {
        return getValueOrNull { splittedData[UV_LIGHT_INTENSITY].toInt() }
    }

    private fun getIlluminance(): Int? {
        return getValueOrNull { splittedData[ILLUMINANCE].toInt() }
    }

    private fun getChecksum(): Int? {
        return getValueOrNull { splittedData[CHECKSUM_INDEX].substring(0,2).toInt(16) }
    }

    private fun isValid(): Boolean {
        return try {
            val sequence = inputAsString.subSequence(1, inputAsString.lastIndexOf('*')).toString()
            calculateChecksum(sequence) == getChecksum()
        } catch (e: Exception) {
            false
        }
    }

    private fun calculateChecksum(sequence: String): Int {
        var checksum = 0
        for (i in 0 until sequence.length) {
            checksum = checksum.xor(sequence[i].toInt())
        }
        return checksum
    }

    private fun <T : Number> getValueOrNull(parser: () -> T): T? {
        return try {
            parser.invoke()
        } catch (ignored: Exception) {
            null
        }
    }
}