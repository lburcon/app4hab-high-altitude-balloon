package pl.edu.pwr.aerospace.app4hab.data.net

import pl.edu.pwr.aerospace.app4hab.data.net.models.App4HabHeadquartersResponseModel
import pl.edu.pwr.aerospace.app4hab.data.net.models.CommandModel
import pl.edu.pwr.aerospace.app4hab.data.net.models.PhotoModel
import pl.edu.pwr.aerospace.app4hab.data.net.models.StatusModel

/**
 * Interface for handling server requests
 *
 */
interface App4HabHeadquartersService {
    fun getCommands(callback: App4HabHeadquartersCallback<CommandModel>)
    fun sendStatus(status: StatusModel, callback: App4HabHeadquartersCallback<App4HabHeadquartersResponseModel>)
    fun sendPhoto(photo: PhotoModel, callback: App4HabHeadquartersCallback<App4HabHeadquartersResponseModel>)
}