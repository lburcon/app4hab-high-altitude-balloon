package pl.edu.pwr.aerospace.app4hab.data.camera

import android.os.Environment
import android.util.Log

import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Implementation of [PhotoSaver].
 * Saves photo in given directory.
 *
 * @param mediaStorageDir
 */
class PhotoSaverImpl(private val mediaStorageDir: File) : PhotoSaver {
    init {
        if (Environment.getExternalStorageState() != Environment.MEDIA_MOUNTED) {
            throw IllegalStateException("External Storage is not writable!")
        }

        if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()) {
            throw IllegalStateException("Failed to create directory")
        }
    }

    companion object {
        private const val DEBUG_TAG: String = "PhotoSaver"
    }

    /**
     * Saves photo in [mediaStorageDir].
     *
     * @param dataSet photo as [ByteArray]
     */
    override fun savePhoto(dataSet: ByteArray): Boolean {
        try {
            val calendar = Calendar.getInstance()
            val dateFormat = SimpleDateFormat("yyyyMMdd_HHmmss")
            val formattedFileName = dateFormat.format(calendar.time)

            val outputFile = File(mediaStorageDir, "$formattedFileName.jpg")
            val outStream = FileOutputStream(outputFile)
            with(outStream) {
                outStream.write(dataSet)
                outStream.close()
            }

            Log.d(DEBUG_TAG,"${dataSet.size} byte written to $mediaStorageDir$formattedFileName.jpg")
            return true
        } catch (e: FileNotFoundException) {
            Log.d(DEBUG_TAG,e.message)
        } catch (e: IOException) {
            Log.d(DEBUG_TAG,e.message)
        }

        return false
    }
}