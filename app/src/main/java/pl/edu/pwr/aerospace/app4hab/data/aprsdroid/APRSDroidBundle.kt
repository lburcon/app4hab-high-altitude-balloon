package pl.edu.pwr.aerospace.app4hab.data.aprsdroid

import java.io.Serializable

/**
 * Default Bundle for [APRSDroidController.sendPackage]
 */
data class APRSDroidBundle(val message: String = "EMPTY_MESSAGE") : Serializable