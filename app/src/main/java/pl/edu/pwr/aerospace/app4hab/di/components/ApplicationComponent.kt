package pl.edu.pwr.aerospace.app4hab.di.components

import dagger.Component
import pl.edu.pwr.aerospace.app4hab.di.modules.*
import pl.edu.pwr.aerospace.app4hab.service.MainService
import javax.inject.Singleton

/**
 * Created by lukasz.burcon on 19.12.2017.
 */
@Singleton
@Component (modules = arrayOf(
        ApplicationModule::class,
        DatabaseModule::class,
        ControllerModule::class,
        NetModule::class,
        FileModule::class,
        CameraCollectorModule::class,
        APRSDroidModule::class,
        NetHandlersModule::class,
        DataCollectorsModule::class,
        UsbReaderModule::class,
        WorkersModule::class))
interface ApplicationComponent {
    fun inject(service: MainService)
}