package pl.edu.pwr.aerospace.app4hab.data.collectors.device

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.BatteryManager
import pl.edu.pwr.aerospace.app4hab.data.database.models.DeviceSensorsModel

/**
 * Class responsible for collecting battery temperature and percentage
 * Class implements [BroadcastReceiver] and [DataCollector] interface
 * @param context main application context
 */
class BatteryDataCollector(val context: Context) : BroadcastReceiver(), DataCollector {

    private var batteryTemp: Float? = null
    private var batteryLevel: Int? = null

    /**
     * batteryTemp is batteryTempAsInt divided by 10 because method returning battery temperature returns integer instead of float (e.g. 257 instead of 25.7)
     */
    override fun onReceive(context: Context?, intent: Intent?) {
        val batteryTempAsInt = intent?.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, 0)
        batteryTemp = batteryTempAsInt?.div(10.0f)
        batteryLevel = intent?.getIntExtra(BatteryManager.EXTRA_LEVEL, 0)
    }

    override fun registerCollector() {
        context.registerReceiver(this, IntentFilter(Intent.ACTION_BATTERY_CHANGED))
    }

    /**
     * Writes battery data to the main phone sensors model
     */
    override fun getData(model: DeviceSensorsModel) {
        model.batteryTemperature = batteryTemp
        model.batteryPercentage = batteryLevel
    }

    override fun unregisterCollector() {
        context.unregisterReceiver(this)
    }
}