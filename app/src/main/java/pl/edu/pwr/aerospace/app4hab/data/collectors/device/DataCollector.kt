package pl.edu.pwr.aerospace.app4hab.data.collectors.device

import pl.edu.pwr.aerospace.app4hab.data.database.models.DeviceSensorsModel

/**
 * Interface implemented by all sensor-specific data collectors.
 * Created mainly for dependency injection purposes.
 */
interface DataCollector {
    /**
     * Registers collector to DataCollector
     */
    fun registerCollector()
    fun unregisterCollector()
    fun getData(model: DeviceSensorsModel)
}