package pl.edu.pwr.aerospace.app4hab

import com.google.gson.annotations.SerializedName

/**
 * Created by przemyslaw.materna on 12.06.2018.
 */
data class SettingsModel(@SerializedName("server_address") val serverAddress: String,
                         @SerializedName("api_key_name") val apiKeyName: String,
                         @SerializedName("api_key_value") val apiKeyValue: String)