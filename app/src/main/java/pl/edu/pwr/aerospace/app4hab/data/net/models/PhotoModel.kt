package pl.edu.pwr.aerospace.app4hab.data.net.models

import com.google.gson.annotations.SerializedName

/**
 * Base photo model
 *
 * @param img encoded image (base64)
 * @param time timestamp of photo
 * @param count
 * @param latitude
 * @param longitude
 * @param altitude
 */
data class PhotoModel(@SerializedName("img") val img: String,
                      @SerializedName("timestamp") val time: Long,
                      @SerializedName("count") val count: Int,
                      @SerializedName("longitude") val longitude: Float?,
                      @SerializedName("latitude") val latitude: Float?,
                      @SerializedName("altitude") val altitude: Int?) : App4HabHeadquartersResponseModel()