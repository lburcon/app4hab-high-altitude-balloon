package pl.edu.pwr.aerospace.app4hab.data.net.models

import com.google.gson.annotations.SerializedName

/**
 * Base command model
 *
 * @param logging determines if atmega should log sensor data to onboard memory
 * @param camera detrmines if camera should be taking photos or not
 * @param radio determines if radio should be transmitting data
 * @param device determines if device should be collecting data
 * @param sendPicture determines if picture should be transmitted via Internet
 */
@Suppress("MemberVisibilityCanPrivate")
data class CommandModel(@SerializedName("logging") val logging: Boolean,
                        @SerializedName("camera") val camera: Boolean,
                        @SerializedName("radio") val radio: Boolean,
                        @SerializedName("device") val device: Boolean,
                        @SerializedName("sendPicture") val sendPicture: Boolean,
                        @SerializedName("id") val id: Int) : App4HabHeadquartersResponseModel()