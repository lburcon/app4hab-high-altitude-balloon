package pl.edu.pwr.aerospace.app4hab.workers

import android.util.Log
import pl.edu.pwr.aerospace.app4hab.data.DataController
import pl.edu.pwr.aerospace.app4hab.data.collectors.device.DeviceSensorsDataCollector
import pl.edu.pwr.aerospace.app4hab.data.threads.DeviceSensorsAsyncTask

/**
 * Worker for device's sensors
 */
@WorkerService(WorkersNames.DeviceSensorsWorker)
class DeviceSensorsWorker(private val sensorsDataCollector: DeviceSensorsDataCollector,
                          private val dataController: DataController) : Worker {

    companion object {
        private const val TAG = "DeviceSensorsWorker"
    }

    @Volatile private var mState: WorkerState = WorkerState.STOPPED

    override fun startWorker(): Boolean {
        return runStandardStartingFlow {
            sensorsDataCollector.registerCollectors()
            DeviceSensorsAsyncTask.startInstance(dataController, sensorsDataCollector)
        }
    }

    override fun stopWorker(): Boolean {
        return runStandardStoppingFlow {
            sensorsDataCollector.unregisterCollectors()
            DeviceSensorsAsyncTask.stopInstance()
        }
    }

    override fun getState(): WorkerState {
        return mState
    }

    private fun runStandardStartingFlow(startingActions: () -> Unit) : Boolean {
        try {
            Log.d(TAG,"Service is starting")
            return if (mState == WorkerState.STOPPED) {
                mState = WorkerState.LAUNCHING
                startingActions.invoke()
                Log.d(TAG,"Service is started")
                mState = WorkerState.RUNNING
                true
            }
            else {
                Log.d(TAG,"Service starting failed.")
                false
            }
        } catch (ex: Exception) {
            Log.d(TAG,"Sth gone wrong.")
            mState = WorkerState.BROKEN
            return false
        }
    }

    private fun runStandardStoppingFlow(stoppingActions: () -> Unit) : Boolean {
        try {
            Log.d(TAG,"Service is stopping")
            return if (mState == WorkerState.RUNNING) {
                mState = WorkerState.STOPPING
                stoppingActions.invoke()
                Log.d(TAG,"Service is stopped")
                mState = WorkerState.STOPPED
                true
            }
            else {
                Log.d(TAG,"Service stopping failed.")
                false
            }
        } catch (ex: Exception) {
            Log.d(TAG,"Sth gone wrong.")
            mState = WorkerState.BROKEN
            return false
        }
    }
}