package pl.edu.pwr.aerospace.app4hab.data.aprsdroid

import android.content.Context
import android.content.Intent
import android.util.Log



/**
 * This interface allows to send messages to APRSDroid application
 */
interface APRSDroidController {
    fun runServiceOnce()
    fun runService()
    fun stopService()
    fun sendPackage(bundle: APRSDroidBundle)
}

/**
 * Default implementation of [APRSDroidController].
 *
 * @author Przemek Materna
 * @see "https://aprsdroid.org/api/"
 *
 * @param context Default context.
 */
class APRSDroidControllerImpl(private val context: Context) : APRSDroidController {
    companion object {
        private val INTENT_PACKAGE = "org.aprsdroid.app"
        private val ACTION_PREFIX = "org.aprsdroid.app."
        private val ACTION_RUN_ONCE = "ONCE"
        private val ACTION_RUN_SERVICE = "SERVICE"
        private val ACTION_STOP_SERVICE = "SERVICE_STOP"
        private val ACTION_SEND_PACKET = "SEND_PACKET"

        private val LOG_PREFIX = "APRSDroid Controller: "
    }

    /**
     * Allows to run service once. After first action service is stopped.
     */
    override fun runServiceOnce() {
        val intent = Intent("$ACTION_PREFIX$ACTION_RUN_ONCE").setPackage(INTENT_PACKAGE)
        Log.d(LOG_PREFIX,"Run service once")
        context.startService(intent)
    }

    /**
     * Allows to run service.
     */
    override fun runService() {
        val intent = Intent("$ACTION_PREFIX$ACTION_RUN_SERVICE").setPackage(INTENT_PACKAGE)
        Log.d(LOG_PREFIX,"Run service")
        context.startService(intent)
    }

    /**
     * Allows to stop service.
     */
    override fun stopService() {
        val intent = Intent("$ACTION_PREFIX$ACTION_STOP_SERVICE").setPackage(INTENT_PACKAGE)
        Log.d(LOG_PREFIX,"Stop service")
        context.startService(intent)
    }

    /**
     * Allows to send package as service. If service is stopped nothing happens.
     *
     * @param bundle message to send (only packet payload). Rest of data are provided by APRSDroid APP.
     */
    override fun sendPackage(bundle: APRSDroidBundle) {
        val intent = Intent("$ACTION_PREFIX$ACTION_SEND_PACKET").setPackage(INTENT_PACKAGE)
        Log.d(LOG_PREFIX,"dataSent: - " + bundle.toString())

        intent.putExtra("data", bundle.message)
        context.startService(intent)
    }

}