package pl.edu.pwr.aerospace.app4hab.di.modules

import android.content.Context
import dagger.Module
import dagger.Provides
import pl.edu.pwr.aerospace.app4hab.data.DataController
import pl.edu.pwr.aerospace.app4hab.data.aprsdroid.APRSDroidControllerImpl
import pl.edu.pwr.aerospace.app4hab.data.atmega.UsbReader

/**
 * Created by lukasz.burcon on 20.03.2018.
 */
@Module(includes = arrayOf(ApplicationModule::class, ControllerModule::class))
class UsbReaderModule {

    @Provides
    fun provideUsbReader(context: Context, dataController: DataController)
            = UsbReader(context, dataController)

}