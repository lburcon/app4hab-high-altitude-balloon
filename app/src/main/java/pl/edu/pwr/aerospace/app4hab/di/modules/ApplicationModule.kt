package pl.edu.pwr.aerospace.app4hab.di.modules

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by lukasz.burcon on 19.12.2017.
 */
@Module
class ApplicationModule(private val application: Application) {

    @Provides
    @Singleton
    fun provideApplicationContext() : Context = application.applicationContext


    @Provides
    @Singleton
    fun provideApplication() : Application = application
}