package pl.edu.pwr.aerospace.app4hab.workers

/**
 * Created by przemyslaw.materna on 30.03.2018.
 */
object WorkersNames {
    const val APRSDroidWorker = "aprsDroid"
    const val AtmegaWorker = "atmega"
    const val DeviceSensorsWorker = "deviceSensors"
    const val CameraWorker = "photo"
}