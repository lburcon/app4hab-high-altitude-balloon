package pl.edu.pwr.aerospace.app4hab.workers

/**
 * Class annotation
 */
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
annotation class WorkerService(val name: String)