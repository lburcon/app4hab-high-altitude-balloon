package pl.edu.pwr.aerospace.app4hab.data.camera

/**
 * Basic interface of Camera Collector
 */
interface CameraCollector {
    fun startCollector()
}
