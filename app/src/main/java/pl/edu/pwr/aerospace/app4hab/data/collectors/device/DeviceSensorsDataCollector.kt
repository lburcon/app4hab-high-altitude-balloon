package pl.edu.pwr.aerospace.app4hab.data.collectors.device

import pl.edu.pwr.aerospace.app4hab.data.database.models.DeviceSensorsModel

/**
 * Interface created for dependency injection purposes.
 */
interface DeviceSensorsDataCollector {
    fun registerCollectors()
    fun unregisterCollectors()
    fun getDeviceSensorsModel(): DeviceSensorsModel
}