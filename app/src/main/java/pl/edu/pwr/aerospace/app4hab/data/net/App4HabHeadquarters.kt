package pl.edu.pwr.aerospace.app4hab.data.net

import pl.edu.pwr.aerospace.app4hab.data.net.models.App4HabHeadquartersResponseModel
import pl.edu.pwr.aerospace.app4hab.data.net.models.CommandModel
import pl.edu.pwr.aerospace.app4hab.data.net.models.PhotoModel
import pl.edu.pwr.aerospace.app4hab.data.net.models.StatusModel
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

/**
 * Interface with commands used for communicating with server.
 *
 */
interface App4HabHeadquarters {
    @GET("api/commands")
    fun getCommands(): Call<CommandModel>

    @POST("api/status")
    fun sendStatus(@Body status: StatusModel): Call<Void>

    @POST("api/photo")
    fun sendPhoto(@Body photo: PhotoModel): Call<Void>
}