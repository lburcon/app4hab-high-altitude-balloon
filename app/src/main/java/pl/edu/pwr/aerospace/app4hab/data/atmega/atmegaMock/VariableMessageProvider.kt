package pl.edu.pwr.aerospace.app4hab.data.atmega.atmegaMock

//"$JP2GD,21,37.7,10.04,9,11.5,20.4,12.3,32.1,1.2,2.2,3.3,4.4,5,6,7.7,8.8*7c";
/*
i
time
lat
lon --
alt
outside temp
inside temp
pressure
battery
current
accx
accy
ozone
rad
pm25
pm10
chksum
 */

/**
 * Created by Grzes on 04.02.2018.
 */
class VariableMessageProvider : MessageProvider() {
    //Deltas describing change of given value between messages
    private val DLatitude = 0.00043f
    private val DLongitude = 0.00071f
    private val DAltitude = 4.81f
    private val DOutsideTemp = -0.03f
    private val DInsideTemp = -0.002f
    private val DPressure = 1.3f
    private val DBattery = 0.027f

    private var invokementCount = 0

    private val time: Int
        get() = invokementCount * 1000

    private val lat: Float
        get() = 51.2f + invokementCount * DLatitude

    private val lon: Float
        get() = 17 + invokementCount * DLongitude

    private val alt: Int
        get() = (213 + invokementCount * DAltitude).toInt()

    private val outTemp: Float
        get() = 21 + invokementCount * DOutsideTemp

    private val insideTemp: Float
        get() = 21 + invokementCount * DInsideTemp

    private val pressure: Float
        get() = 1021 + invokementCount * DPressure

    private val battery: Float
        get() = 5.03f + invokementCount * DBattery

    private val current: Int
        get() = 1012

    private val accX: Float
        get() = (rand.nextDouble() * 4).toFloat()

    private val accY: Float
        get() = (rand.nextDouble() * 4).toFloat()

    private val accZ: Float
        get() = (rand.nextDouble() * 4).toFloat()

    override fun message(): String {
        invokementCount += 1
        var res = "\$RKSHD,"
        res += invokementCount.toString() + ","
        res += time.toString() + ","
        res += lat.toString() + ","
        res += lon.toString() + ","
        res += alt.toString() + ","
        res += outTemp.toString() + ","
        res += insideTemp.toString() + ","
        res += pressure.toString() + ","
        res += battery.toString() + ","
        res += current.toString() + ","
        res += accX.toString() + ","
        res += accY.toString() + ","
        res += accZ.toString()
        res += "*" + getChecksum(res)

        return res
    }

    private fun getChecksum(string: String): String {
        var checksum = 0
        for (i in 1 until string.length) {
            checksum = checksum.xor(string[i].toInt())
        }
        return checksum.toString(16)
    }
}
