package pl.edu.pwr.aerospace.app4hab.ui

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_main.*
import pl.edu.pwr.aerospace.app4hab.R

/**
 * Created by t-rex on 31/05/2018.
 */
interface MainView {

}

class MainFragment : Fragment(), MainView {
    companion object {
        fun instance(): MainFragment {
            return MainFragment()
        }

        private const val REMOTE_OFF_VIEW = 0
        private const val REMOTE_ON_VIEW = 1
    }

    private val presenter: MainFragmentPresenter by lazy {
        val service = (activity as? MainActivity)?.serviceInstance ?: throw IllegalStateException("Unknown type of activity")
        MainFragmentPresenterImpl(this, service)
    }
    private lateinit var adapter: WorkerListAdapter

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        initView()
    }

    private fun initView() {
        initMainSwitch()
        initWorkersRecyclerView()

        this.change_workers_confirmation.setOnClickListener {
            presenter.updateWorkers()
            adapter.notifyDataSetChanged()
        }
    }

    private fun initMainSwitch() {
        this.main_switch.isChecked = presenter.isRemoteControlSwitchChecked()
        this.main_switch.setOnCheckedChangeListener { _, isChecked ->
            this.main_view_animator.displayedChild = if (isChecked) REMOTE_ON_VIEW else REMOTE_OFF_VIEW
            presenter.remoteControlSwitchCheckedChanged(isChecked)
        }
    }

    private fun initWorkersRecyclerView() {
        val workerModels = presenter.getWorkers().map { item ->
            WorkerListAdapter.WorkerSwitchModel(
                    worker = item.key,
                    qualifiedName = item.key.getQualifiedName(),
                    action = { worker, isChecked ->
                        presenter.changeWorkerState(worker, isChecked)
                    }
            )
        }

        adapter = WorkerListAdapter(workerModels)
        this.workers_list_recycler_view.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = this@MainFragment.adapter
        }
        adapter.notifyDataSetChanged()
    }
}