package pl.edu.pwr.aerospace.app4hab.workers

import android.content.Context
import android.util.Log
import pl.edu.pwr.aerospace.app4hab.data.camera.CameraCollector
import pl.edu.pwr.aerospace.app4hab.data.threads.CameraAsyncTask

/**
 * Worker for camera
 */
@WorkerService(WorkersNames.CameraWorker)
class CameraWorker(private val cameraCollector: CameraCollector, private val context: Context) : Worker {
    companion object {
        private const val DEBUG_TAG = "CameraWorker"
    }

    @Volatile private var mState: WorkerState = WorkerState.STOPPED

    override fun stopWorker(): Boolean {
        return runStandardStoppingFlow {
            CameraAsyncTask.stopInstance()
        }
    }

    override fun startWorker(): Boolean {
        return runStandardStartingFlow {
            CameraAsyncTask.startInstance(cameraCollector)
        }
    }

    override fun getState(): WorkerState {
        return mState
    }

    private fun runStandardStartingFlow(startingActions: () -> Unit) : Boolean {
        try {
            Log.d(DEBUG_TAG,"Service is starting")
            return if (mState == WorkerState.STOPPED) {
                mState = WorkerState.LAUNCHING
                startingActions.invoke()
                Log.d(DEBUG_TAG,"Service is started")
                mState = WorkerState.RUNNING
                true
            }
            else {
                Log.d(DEBUG_TAG,"Service starting failed.")
                false
            }
        } catch (ex: Exception) {
            Log.d(DEBUG_TAG,"Sth gone wrong.")
            mState = WorkerState.BROKEN
            return false
        }
    }

    private fun runStandardStoppingFlow(stoppingActions: () -> Unit) : Boolean {
        try {
            Log.d(DEBUG_TAG,"Service is stopping")
            return if (mState == WorkerState.RUNNING) {
                mState = WorkerState.STOPPING
                stoppingActions.invoke()
                Log.d(DEBUG_TAG,"Service is stopped")
                mState = WorkerState.STOPPED
                true
            }
            else {
                Log.d(DEBUG_TAG,"Service stopping failed.")
                false
            }
        } catch (ex: Exception) {
            Log.d(DEBUG_TAG,"Sth gone wrong.")
            mState = WorkerState.BROKEN
            return false
        }
    }
}