package pl.edu.pwr.aerospace.app4hab.data.threads

import android.os.AsyncTask
import android.util.Log
import pl.edu.pwr.aerospace.app4hab.data.DataController
import pl.edu.pwr.aerospace.app4hab.data.aprsdroid.APRSDroidBundle
import pl.edu.pwr.aerospace.app4hab.data.aprsdroid.APRSDroidController

/**
 * AsyncTask for communication using APRSdroid
 */
class APRSDroidAsyncTask private constructor(private val dataController: DataController,
                                             private val mAprsDroidController: APRSDroidController) : AsyncTask<Void, Void, Void>() {
    companion object {
        const val INTERVAL_TIME = 180000L

        private var instance: APRSDroidAsyncTask? = null
        private const val DEBUG_TAG = "APRSDroidAsyncTask"

        fun stopInstance() {
            synchronized(this) {
                instance?.cancel(true)
                instance = null
            }
        }

        fun startInstance(dataController: DataController, aprsDroidController: APRSDroidController) {
            synchronized(this) {
                if (instance != null) {
                    stopInstance()
                }
                instance = APRSDroidAsyncTask(dataController, aprsDroidController)
                instance?.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
            }
        }
    }

    /**
     * Sends packages to APRSDroid application
     */
    override fun doInBackground(vararg params: Void?): Void? {
        Log.d(DEBUG_TAG,"background")
        val message = dataController.getAPRSDroidMessage()
        mAprsDroidController.sendPackage(APRSDroidBundle(message))
        Log.d(DEBUG_TAG,"package send: $message")
        Thread.sleep(INTERVAL_TIME)
        return null
    }

    /**
     * Start new instance of this class in order to start looping
     */
    override fun onPostExecute(result: Void?) {
        super.onPostExecute(result)
        synchronized(this) {
            instance = APRSDroidAsyncTask(dataController, mAprsDroidController)
            instance?.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
        }
        Log.d(DEBUG_TAG,"onPost")
    }

    /**
     * Handle cancelling of this AsyncTask
     */
    override fun onCancelled() {
        Log.d(DEBUG_TAG,"onCancel")
        this.cancel(true)
        super.onCancelled()
    }
}