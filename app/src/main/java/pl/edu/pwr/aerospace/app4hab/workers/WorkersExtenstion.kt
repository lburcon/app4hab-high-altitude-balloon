package pl.edu.pwr.aerospace.app4hab.workers

import kotlin.reflect.KClass

/**
 * @return Returns worker's name
 */
fun Worker.getWorkerName(): String {
    return (this.javaClass.annotations.find { it is WorkerService } as? WorkerService)?.name
            ?: "This is not a proper worker!"
}

fun <T: Worker> KClass<T>.getWorkerName(): String {
    return (this.annotations.find { it is WorkerService } as? WorkerService)?.name
            ?: "This is not a proper worker!"
}