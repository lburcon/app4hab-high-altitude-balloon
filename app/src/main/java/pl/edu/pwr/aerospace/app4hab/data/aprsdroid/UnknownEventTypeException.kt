package pl.edu.pwr.aerospace.app4hab.data.aprsdroid

/**
 * Exeption thrown when application cannot handle [APRS_DROID_UPDATE_TYPE]
 */
class UnknownEventTypeException(val code: Int) : Exception()