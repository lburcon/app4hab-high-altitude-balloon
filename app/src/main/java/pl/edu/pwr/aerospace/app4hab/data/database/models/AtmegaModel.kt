package pl.edu.pwr.aerospace.app4hab.data.database.models

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey


/**
 * Atmega data class model
 */
@Entity(tableName = "atmega_data")
data class AtmegaModel(
        @ColumnInfo(name = "raw") var original: String = "",
        @ColumnInfo(name = "time") var absoluteTime: Long = System.currentTimeMillis(),
        @ColumnInfo(name = "number") var number: Int? = null,
        @ColumnInfo(name = "relative_time") var relativeTime: Int? = null,
        @ColumnInfo(name = "outside_temperature") var out_temp: Float? = null,
        @ColumnInfo(name = "inside_temperature") var inTemp: Float? = null,
        @ColumnInfo(name = "pressure") var pressure: Float? = null,
        @ColumnInfo(name = "battery_voltage") var batteryVoltage: Float? = null,
        @ColumnInfo(name = "current_usage") var currentUsage: Int? = null,
        @ColumnInfo(name = "acc_x") var accX: Float? = null,
        @ColumnInfo(name = "acc_y") var accY: Float? = null,
        @ColumnInfo(name = "acc_z") var accZ: Float? = null,
        @ColumnInfo(name = "longitude") var longitude: Float? = null,
        @ColumnInfo(name = "latitude") var latitude: Float? = null,
        @ColumnInfo(name = "altitude") var altitude: Int? = null,
        @ColumnInfo(name = "uv_light_intensity") var uvLightIntensity: Int? = null,
        @ColumnInfo(name = "illuminance") var illuminance: Int? = null,
        @ColumnInfo(name = "checksum") var checksum: Int? = null,
        @ColumnInfo(name = "valid") var isValid: Boolean = false     //Room doesn't allow variable name to start with 'is' (like isValid)
) {
    @ColumnInfo(name = "id")
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
}