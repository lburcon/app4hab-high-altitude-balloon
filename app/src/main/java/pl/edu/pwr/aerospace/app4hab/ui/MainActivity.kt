package pl.edu.pwr.aerospace.app4hab.ui

import android.Manifest
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.IBinder
import android.os.Environment
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import org.jetbrains.anko.toast
import pl.edu.pwr.aerospace.app4hab.R
import pl.edu.pwr.aerospace.app4hab.service.MainService
import java.io.File
import java.io.IOException


/**
 * Main Activity for initializing services with business logic
 */
class MainActivity : AppCompatActivity() {

    companion object {
        /**
         * Used for debug purposes
         */
        private const val IS_DEBUG_MODE = false
    }

    private val serviceIntent by lazy { Intent(applicationContext, MainService::class.java) }
    var serviceInstance: MainService? = null
        private set

    private val mConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceDisconnected(name: ComponentName?) {
            toast("Service disconnected")
        }

        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            val binder = service as? MainService.MainServiceLocalBinder ?: return
            serviceInstance = binder.getServiceInstance()
            injectFragment()
            toast("Service connected")
        }

    }

    /**
     * Main onCreate of App4HAB application. Initializes services  and debug
     * mode, or starts asking for permissions if they haven't been granted yet
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (getPermissions()) {
            startService()
            setupLoggingToFile()
            checkDebugMode()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        unbindService(mConnection)
        serviceInstance = null
    }

    private fun injectFragment() {
        supportFragmentManager.beginTransaction()
                .replace(R.id.content_frame, MainFragment.instance())
                .commitAllowingStateLoss()
    }

    /**
     * Permissions granting callback.
     * See {@link #getPermissions()}.
     */
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        startService()
        checkDebugMode()
    }

    /**
     * Start main service that holds AsyncTasks with collectors
     */
    private fun startService() {
        startService(serviceIntent)
        bindService(serviceIntent, mConnection, Context.BIND_AUTO_CREATE)
    }

    /**
     * Asks for permissions needed to make photos and write them onto our phone.
     */
    private fun getPermissions(): Boolean {
        var arePermissionsGranted = true
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {

            arePermissionsGranted = false
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE), 50)
            this.toast(getString(pl.edu.pwr.aerospace.app4hab.R.string.permission_reset_app))
        }

        return arePermissionsGranted
    }

    /**
     * Used for debug purposes
     */
    private fun checkDebugMode() {
        if (IS_DEBUG_MODE) {
            //startActivity<APRSDroidActivity>()
        }
    }

    private fun setupLoggingToFile() {
        val logFile = File(Environment.getExternalStorageDirectory(), "logcat" + System.currentTimeMillis() + ".txt")
        // clear the previous logcat and then write the new one to the file
        try {
            Runtime.getRuntime().exec("logcat -c")
            Runtime.getRuntime().exec("logcat -f $logFile")
        } catch (e: IOException) {
            e.printStackTrace()
        }

    }
}
