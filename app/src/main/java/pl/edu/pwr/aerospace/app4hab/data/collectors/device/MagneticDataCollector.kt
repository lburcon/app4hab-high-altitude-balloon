package pl.edu.pwr.aerospace.app4hab.data.collectors.device

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import pl.edu.pwr.aerospace.app4hab.data.database.models.DeviceSensorsModel

/**
 * Class responsible for collecting magnetic field data in 3 axes: X, Y, Z
 * Class implements [SensorEventListener] and [DataCollector] interface
 * @param context main application context
 */
class MagneticDataCollector(context: Context) : SensorEventListener, DataCollector {

    private var magneticFieldX: Float? = null
    private var magneticFieldY: Float? = null
    private var magneticFieldZ: Float? = null

    private val sensorManager: SensorManager by lazy {
        context.getSystemService(Context.SENSOR_SERVICE) as SensorManager
    }

    override fun onAccuracyChanged(p0: Sensor?, p1: Int) {
    }

    override fun onSensorChanged(event: SensorEvent?) {
        magneticFieldX = event?.values?.get(0)
        magneticFieldY = event?.values?.get(1)
        magneticFieldZ = event?.values?.get(2)
    }

    override fun registerCollector() {
        sensorManager.registerListener(
                this,
                sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),
                SensorManager.SENSOR_DELAY_NORMAL
        )
    }

    /**
     * Writes magnetic field data to the main phone sensors model
     */
    override fun getData(model: DeviceSensorsModel) {
        model.magneticFieldX = magneticFieldX
        model.magneticFieldY = magneticFieldY
        model.magneticFieldZ = magneticFieldZ
    }

    override fun unregisterCollector() {
        sensorManager.unregisterListener(this)
    }
}