package pl.edu.pwr.aerospace.app4hab.workers

import android.content.Context
import android.util.Log
import pl.edu.pwr.aerospace.app4hab.data.DataController
import pl.edu.pwr.aerospace.app4hab.data.aprsdroid.APRSDroidController
import pl.edu.pwr.aerospace.app4hab.data.aprsdroid.APRSDroidEventReceiver
import pl.edu.pwr.aerospace.app4hab.data.threads.APRSDroidAsyncTask

/**
 * Worker for APRSDroid
 */
@WorkerService(WorkersNames.APRSDroidWorker)
class APRSDroidWorker(private val aprsReceiver: APRSDroidEventReceiver,
                      private val aprsDroidController: APRSDroidController,
                      private val dataController: DataController,
                      private val context: Context) : Worker {
    companion object {
        private const val TAG = "APRSDroidWorker"
    }

    @Volatile private var mState: WorkerState = WorkerState.STOPPED

    override fun startWorker(): Boolean {
        return runStandardStartingFlow {
            aprsReceiver.registerReceiver(context)
            aprsDroidController.runService()
            APRSDroidAsyncTask.startInstance(dataController, aprsDroidController)
        }
    }

    override fun stopWorker(): Boolean {
        return runStandardStoppingFlow {
            aprsReceiver.unregisterReceiver(context)
            aprsDroidController.stopService()
            APRSDroidAsyncTask.stopInstance()
        }
    }

    override fun getState(): WorkerState {
        return mState
    }

    private fun runStandardStartingFlow(startingActions: () -> Unit) : Boolean {
        try {
            Log.d(TAG,"Service is starting")
            return if (mState == WorkerState.STOPPED) {
                mState = WorkerState.LAUNCHING
                startingActions.invoke()
                Log.d(TAG,"Service is started")
                mState = WorkerState.RUNNING
                true
            }
            else {
                Log.d(TAG,"Service starting failed.")
                false
            }
        } catch (ex: Exception) {
            Log.d(TAG,"Sth gone wrong.")
            mState = WorkerState.BROKEN
            return false
        }
    }

    private fun runStandardStoppingFlow(stoppingActions: () -> Unit) : Boolean {
        try {
            Log.d(TAG,"Service is stopping")
            return if (mState == WorkerState.RUNNING) {
                mState = WorkerState.STOPPING
                stoppingActions.invoke()
                Log.d(TAG,"Service is stopped")
                mState = WorkerState.STOPPED
                true
            }
            else {
                Log.d(TAG,"Service stopping failed.")
                false
            }
        } catch (ex: Exception) {
            Log.d(TAG,"Sth gone wrong.")
            mState = WorkerState.BROKEN
            return false
        }
    }
}