package pl.edu.pwr.aerospace.app4hab.di.modules

import dagger.Module
import dagger.Provides
import pl.edu.pwr.aerospace.app4hab.data.DataController
import pl.edu.pwr.aerospace.app4hab.data.database.AppDatabase

/**
 * Created by lukasz.burcon on 20.12.2017.
 */
@Module(includes = arrayOf(DatabaseModule::class))
class ControllerModule {

    @Provides
    fun provideDatabaseController(database: AppDatabase): DataController = DataController(database)
}