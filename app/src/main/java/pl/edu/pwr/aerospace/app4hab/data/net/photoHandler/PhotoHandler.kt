package pl.edu.pwr.aerospace.app4hab.data.net.photoHandler


/**
 * Class used to creating and sending photos to external server
 */
interface PhotoHandler {
    /**
     * Sends last photo to server
     */
    fun sendLastPhoto()
}