package pl.edu.pwr.aerospace.app4hab.workers

/**
 * Worker interface
 */
interface Worker {
    /**
     * Stops worker
     */
    fun stopWorker(): Boolean

    /**
     * Starts worker
     */
    fun startWorker(): Boolean

    /**
     * @return worker's state
     * @see [WorkerState]
     */
    fun getState(): WorkerState

    fun getQualifiedName(): String {
        return this.getWorkerName()
    }
}
