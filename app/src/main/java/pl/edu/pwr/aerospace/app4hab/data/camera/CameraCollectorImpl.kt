@file:Suppress("DEPRECATION")

package pl.edu.pwr.aerospace.app4hab.data.camera

import android.content.Context
import android.graphics.SurfaceTexture
import android.hardware.Camera
import android.os.StrictMode
import android.util.Log

import java.io.IOException

/**
 * Implementation of [CameraCollector]].
 *
 * @param cameraConfigurator object for setting photo quality, res etc.
 * @param saver object for saving taken photo
 */
class CameraCollectorImpl(private val camera: Camera,
                          private val cameraConfigurator: CameraConfigurator,
                          private val saver: PhotoSaver) : CameraCollector {
    companion object {
        private const val DEBUG_TAG = "CameraService"
    }

    private val autoFocusCallback = Camera.AutoFocusCallback { success, camera ->
        Log.d(DEBUG_TAG,"Auto focus successful: $success")
        if (success) {
            camera.takePicture(null, null, null, pictureCallback)
            Log.d(DEBUG_TAG,"End of the process.")
        } else
            Log.w(DEBUG_TAG,"Focus couldn't be reached!")
    }

    private val pictureCallback = Camera.PictureCallback { dataSet, _ ->
        Log.d(DEBUG_TAG,"Photo taken!")
        saver.savePhoto(dataSet)
        camera.stopPreview()
    }

    /**
     * Runs collector.
     */
    override fun startCollector() {
        Log.d(DEBUG_TAG,"CameraConfigurator service created")

        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        startWorking()
    }

    private fun startWorking() {
        try {
            camera.setPreviewTexture(SurfaceTexture(Context.MODE_PRIVATE))
            camera.startPreview()
            Log.d(DEBUG_TAG,"Preview started.")
            camera.parameters = cameraConfigurator.configureCamera(camera.parameters)
            camera.autoFocus(autoFocusCallback)
        } catch (e: IOException) {
            Log.d(DEBUG_TAG,"Service failed")
            // TODO try re-init camera
        }
    }

}