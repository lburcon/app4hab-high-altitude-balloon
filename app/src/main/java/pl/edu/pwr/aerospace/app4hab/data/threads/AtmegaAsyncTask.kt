package pl.edu.pwr.aerospace.app4hab.data.threads

import android.os.AsyncTask
import android.util.Log
import pl.edu.pwr.aerospace.app4hab.data.DataController
import pl.edu.pwr.aerospace.app4hab.data.atmega.AtmegaParserImpl
import pl.edu.pwr.aerospace.app4hab.data.database.models.AtmegaModel


/**
 * AsyncTask which reads data from Atmega, parses it and saves to the database
 * every specified time interval
 *
 */
class AtmegaAsyncTask(private val mDataController: DataController,
                      message: ByteArray) : AsyncTask<Void, Void, Void>() {
    private val parser = AtmegaParserImpl(message)
    companion object {
        const val DEBUG_TAG = "AtmegaAsyncTask"
    }
    /**
     * Saves data received from atmega to database
     */
    override fun doInBackground(vararg params: Void?): Void? {
        Log.d(DEBUG_TAG,"background")
        saveToDatabase(parser.getAtmegaModel())
        return null
    }

    /**
     * Save AtmegaModel to database
     *
     * @param atmegaModel model to save
     */
    private fun saveToDatabase(atmegaModel: AtmegaModel) {
        mDataController.addAtmegaModelToDatabase(atmegaModel)
        Log.d(DEBUG_TAG,"Atmega data saved to Database")
    }

    /**
     * Start new instance of this class in order to start looping
     */
    override fun onPostExecute(result: Void?) {
        super.onPostExecute(result)
        Log.d(DEBUG_TAG,"onPost")
    }

    /**
     * Handle cancelling of this AsyncTask
     */
    override fun onCancelled() {
        Log.d(DEBUG_TAG,"onCancel")
        this.cancel(true)
        super.onCancelled()
    }
}