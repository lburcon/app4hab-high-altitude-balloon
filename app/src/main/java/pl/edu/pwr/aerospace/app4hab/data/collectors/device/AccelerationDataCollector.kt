package pl.edu.pwr.aerospace.app4hab.data.collectors.device

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import pl.edu.pwr.aerospace.app4hab.data.database.models.DeviceSensorsModel

/**
 * Class responsible for collecting acceleration data in 3 axes: X, Y, Z
 * Class implements [SensorEventListener] and [DataCollector] interface
 * @param context main application context
 */
class AccelerationDataCollector(context: Context) : SensorEventListener, DataCollector {

    private var accelerationX: Float? = null
    private var accelerationY: Float? = null
    private var accelerationZ: Float? = null

    private val sensorManager: SensorManager by lazy {
        context.getSystemService(Context.SENSOR_SERVICE) as SensorManager
    }

    override fun registerCollector() {
        sensorManager.registerListener(
                this,
                sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL
        )
    }

    override fun onAccuracyChanged(p0: Sensor?, p1: Int) {
    }

    override fun onSensorChanged(event: SensorEvent?) {
        accelerationX = event?.values?.get(0)
        accelerationY = event?.values?.get(1)
        accelerationZ = event?.values?.get(2)
    }

    /**
     * Writes acceleration data to the main phone sensors model
     */
    override fun getData(model: DeviceSensorsModel) {
        model.accelerationX = accelerationX
        model.accelerationY = accelerationY
        model.accelerationZ = accelerationZ
    }

    override fun unregisterCollector() {
        sensorManager.unregisterListener(this)
    }
}