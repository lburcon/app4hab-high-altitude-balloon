package pl.edu.pwr.aerospace.app4hab.data.collectors.device

import pl.edu.pwr.aerospace.app4hab.data.database.models.DeviceSensorsModel

/**
 * Class responsible for using sensor-specific data collectors to read and return new [DeviceSensorsModel]
 * @param collectors list of data collectors that will allow DeviceSensorsDataCollector to gather all necessary data
 */
class DeviceSensorsDataCollectorImpl(vararg val collectors: DataCollector) : DeviceSensorsDataCollector {
    override fun registerCollectors() {
        collectors.forEach { collector -> collector.registerCollector() }
    }

    override fun unregisterCollectors() {
        collectors.forEach { collector -> collector.unregisterCollector() }
    }

    override fun getDeviceSensorsModel(): DeviceSensorsModel {
        val model = DeviceSensorsModel(time = System.currentTimeMillis())
        collectors.forEach { collector -> collector.getData(model) }
        return model
    }
}