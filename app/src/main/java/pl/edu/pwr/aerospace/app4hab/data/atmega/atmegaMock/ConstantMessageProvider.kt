package pl.edu.pwr.aerospace.app4hab.data.atmega.atmegaMock

/**
 * Created by Grzes on 04.02.2018.
 */
/**
 * Used only in tests to generate constant message
 * TODO: it should be deprecated
 */
class ConstantMessageProvider : MessageProvider() {
    override fun message(): String {
        return "\$JP2GD,11,21,37.7,10.04,9,11.5,20.4,12.3,32.1,1.2,2.2,3.3,4.4,5,6,7.7,8.8*7c"
    }
}
