package pl.edu.pwr.aerospace.app4hab.service

import android.util.Log
import org.jetbrains.anko.doAsync
import pl.edu.pwr.aerospace.app4hab.data.net.App4HabHeadquartersCallback
import pl.edu.pwr.aerospace.app4hab.data.net.App4HabHeadquartersService
import pl.edu.pwr.aerospace.app4hab.data.net.models.App4HabHeadquartesException
import pl.edu.pwr.aerospace.app4hab.data.net.models.CommandModel
import pl.edu.pwr.aerospace.app4hab.data.threads.RetrofitAsyncTask
import pl.edu.pwr.aerospace.app4hab.data.net.photoHandler.PhotoHandler
import pl.edu.pwr.aerospace.app4hab.data.net.statusHandler.StatusHandler
import pl.edu.pwr.aerospace.app4hab.workers.APRSDroidWorker
import pl.edu.pwr.aerospace.app4hab.workers.AtmegaWorker
import pl.edu.pwr.aerospace.app4hab.workers.CameraWorker
import pl.edu.pwr.aerospace.app4hab.workers.DeviceSensorsWorker

/**
 * Controller for mainService
 */
class MainServiceController(private val mMainService: MainService,
                            private val mPhoneStateListener: MyPhoneStateListener,
                            private val mNetService: App4HabHeadquartersService,
                            private val photoHandler: PhotoHandler,
                            private val statusHandler: StatusHandler) {
    companion object {
        val DEBUG_TAG = "MainServiceController"
    }

    @Volatile private var mIsRunning = false
    private var mLastResponseId: Int = -1

    private val mPhoneStateChangedListener = object : MyPhoneStateListener.OnMinimalStrengthAchievedListener {
        override fun onAboveMinimal() {
            if (!mIsRunning) {
                scheduleRemoteTask()
            }
        }

        override fun onBelowMinimal() {
            if (mIsRunning) {
                cancelRemoteTask()
            }
        }
    }

    init {
        mPhoneStateListener.apply {
            mStrengthListener = mPhoneStateChangedListener
        }
    }

    private fun sendRequest() {
        mNetService.getCommands(object: App4HabHeadquartersCallback<CommandModel> {
            override fun onSuccess(responseModel: CommandModel) {
                Log.d(DEBUG_TAG,responseModel.toString())
                if (mLastResponseId != responseModel.id) {
                    mLastResponseId = responseModel.id
                    fixWorkerStates(responseModel)

                    if (responseModel.sendPicture) {
                        // Send photo
                        doAsync {
                            photoHandler.sendLastPhoto()
                        }
                        doAsync {
                            statusHandler.sendStatus()
                        }
                    }
                }
            }

            override fun onFail(reason: App4HabHeadquartesException) {
                Log.d(DEBUG_TAG,"request failed")
            }
        })
    }

    private fun fixWorkerStates(commands: CommandModel) {
        val workerStateMap = mapOf(
                Pair(AtmegaWorker::class, commands.logging),
                Pair(APRSDroidWorker::class, commands.radio),
                Pair(DeviceSensorsWorker::class, commands.device),
                Pair(CameraWorker::class, commands.camera)
        )

        mMainService.changeWorkersState(workerStateMap)
        mMainService.printWorkersState()
    }

    fun cancelRemoteTask() {
        synchronized(this) {
            Log.d(DEBUG_TAG,"Canceling send-request task...")
            mIsRunning = false
            mPhoneStateListener.stopListening()
            RetrofitAsyncTask.stopInstance()
        }
    }

    fun scheduleRemoteTask() {
        Log.d(DEBUG_TAG, "Scheduling send-request task...")
        mIsRunning = true
        mPhoneStateListener.startListening()
        RetrofitAsyncTask.startInstance {
            sendRequest()
        }
    }
}