package pl.edu.pwr.aerospace.app4hab.data.aprsdroid

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.location.Location

/**
 * Class gives possibilities to handle APRSDroidEvents [APRS_DROID_UPDATE_TYPE]
 */
class APRSDroidEventReceiver : BroadcastReceiver() {
    companion object {
        private const val APRS_DROID_PREFIX = "org.aprsdroid.app."

        private const val ACTION_SERVICE_STARTED = "SERVICE_STARTED"
        private const val ACTION_SERVICE_STOPPED = "SERVICE_STOPPED"
        private const val ACTION_MESSAGE = "MESSAGE"
        private const val ACTION_POSITION = "POSITION"
        private const val ACTION_UPDATE = "UPDATE"
    }

    /**
     * Used for saving state if receiver is registered in runtime
     */
    var isRegistered = false
        private set

    var aprsDroidRunning = false
        private set

    var eventHandler: APRSDroidEventHandler = APRSDroidEventHandlerImpl()

    /**
     * USE ONLY IF YOU DIDN'T REGISTER BROADCAST IN MANIFEST!
     *
     * Functions allows to register receiver at runtime.
     * Function is not protected from registering service in different context!
     *
     */
    fun registerReceiver(context: Context): Intent? {
        try {
            val intentFilter = IntentFilter()
            intentFilter.addAction("$APRS_DROID_PREFIX$ACTION_SERVICE_STARTED")
            intentFilter.addAction("$APRS_DROID_PREFIX$ACTION_SERVICE_STOPPED")
            intentFilter.addAction("$APRS_DROID_PREFIX$ACTION_MESSAGE")
            intentFilter.addAction("$APRS_DROID_PREFIX$ACTION_POSITION")
            intentFilter.addAction("$APRS_DROID_PREFIX$ACTION_UPDATE")
            return context.registerReceiver(this, intentFilter)
        } finally {
            isRegistered = true
        }
    }

    /**
     * Function allows to unregister register at runtime.
     * Must be called from context that registered receiver.
     */
    fun unregisterReceiver(context: Context) {
        if (isRegistered) {
            context.unregisterReceiver(this)
            isRegistered = false
        }
    }

    /**
     * Handles actions from broadcast receiver.
     * Gets extra intents from Intent and calls methods from [eventHandler]
     */
    override fun onReceive(context: Context?, intent: Intent?) {
        if (!intent!!.action.startsWith(APRS_DROID_PREFIX)) {
            eventHandler.onInternalError("ActionIntent does not start with $APRS_DROID_PREFIX!")
            return
        }

        val action = intent.action.replace(APRS_DROID_PREFIX, "")
        when (action) {
            ACTION_SERVICE_STARTED -> {
                val apiVersion = intent.getIntExtra("api_version", 0)
                val callsign = intent.getStringExtra("callsign")

                aprsDroidRunning = true
                eventHandler.onServiceStarted(apiVersion, callsign)
            }
            ACTION_SERVICE_STOPPED -> {
                aprsDroidRunning = false
                eventHandler.onServiceStopped()
            }
            ACTION_MESSAGE -> {
                val source = intent.getStringExtra("source")
                val dest = intent.getStringExtra("dest")
                val body = intent.getStringExtra("body")
                eventHandler.onMessage(source, dest, body)
            }
            ACTION_POSITION -> {
                val source = intent.getStringExtra("source")
                val callsign = intent.getStringExtra("callsign")
                val location = intent.getParcelableExtra<Location>("location")
                eventHandler.onPosition(source, callsign, location)
            }
            ACTION_UPDATE -> {
                val type = intent.getIntExtra("type", -1)
                val status = intent.getStringExtra("status")
                eventHandler.onUpdate(type, status)
            }
            else -> {
                eventHandler.onInternalError("Unsupported action!")
            }
        }
    }
}