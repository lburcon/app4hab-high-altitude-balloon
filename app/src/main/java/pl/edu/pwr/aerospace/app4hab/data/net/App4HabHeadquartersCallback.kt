package pl.edu.pwr.aerospace.app4hab.data.net

import pl.edu.pwr.aerospace.app4hab.data.net.models.App4HabHeadquartersResponseModel
import pl.edu.pwr.aerospace.app4hab.data.net.models.App4HabHeadquartesException

/**
 * Callback for handling service responses
 *
 */
interface App4HabHeadquartersCallback<in T : App4HabHeadquartersResponseModel> {
    fun onSuccess(responseModel: T)
    fun onFail(reason: App4HabHeadquartesException)
}