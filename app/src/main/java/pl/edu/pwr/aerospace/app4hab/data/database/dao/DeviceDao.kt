package pl.edu.pwr.aerospace.app4hab.data.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import pl.edu.pwr.aerospace.app4hab.data.database.models.AtmegaModel
import pl.edu.pwr.aerospace.app4hab.data.database.models.DeviceSensorsModel

/**
 * Created by Damian Niemiec on 12.03.2018.
 */
@Dao
interface DeviceDao {
    @Query("select * from device_data")
    fun getAllDeviceData(): List<DeviceSensorsModel>

    @Query("SELECT * FROM device_data WHERE id = (SELECT MAX(id) FROM device_data)")
    fun getLastMeasurement(): List<DeviceSensorsModel>

    @Insert
    fun insertDeviceSensorsModel(deviceSensorsModel: DeviceSensorsModel)
}