package pl.edu.pwr.aerospace.app4hab.ui

import pl.edu.pwr.aerospace.app4hab.service.MainService
import pl.edu.pwr.aerospace.app4hab.workers.Worker
import pl.edu.pwr.aerospace.app4hab.workers.WorkerState
import kotlin.reflect.KClass

/**
 * Created by t-rex on 31/05/2018.
 */
interface MainFragmentPresenter {
    fun getWorkers() : Map<Worker, Boolean>
    fun changeWorkerState(worker: Worker, checked: Boolean)
    fun remoteControlSwitchCheckedChanged(checked: Boolean)
    fun isRemoteControlSwitchChecked(): Boolean
    fun updateWorkers()
}

class MainFragmentPresenterImpl(private val view: MainView, private val mainService: MainService): MainFragmentPresenter {

    @Volatile private var remoteControlActive: Boolean = false
    private val workerSwitchMap = mutableMapOf<Worker, Boolean>()

    override fun remoteControlSwitchCheckedChanged(checked: Boolean) {
        remoteControlActive = checked
        mainService.setRemoteControlActive(checked, getStateMap())
    }

    private fun getStateMap(): Map<KClass<out Worker>, Boolean>? {
        return if (remoteControlActive) null else {
            workerSwitchMap.mapKeys { x -> x.key::class }
        }
    }

    override fun updateWorkers() {
        remoteControlSwitchCheckedChanged(false)
    }

    override fun isRemoteControlSwitchChecked(): Boolean {
        return remoteControlActive
    }

    override fun getWorkers(): Map<Worker, Boolean> {
        return mainService.getWorkers().map { worker ->
            worker to (worker.getState() == WorkerState.RUNNING)
        }.toMap()
    }

    override fun changeWorkerState(worker: Worker, checked: Boolean) {
        workerSwitchMap[worker] = checked
    }
}