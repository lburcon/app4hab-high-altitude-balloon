package pl.edu.pwr.aerospace.app4hab.data.threads

import android.os.AsyncTask
import android.util.Log
import pl.edu.pwr.aerospace.app4hab.data.camera.CameraCollector


/**
 * AsyncTask which collects photos every specified time interval
 *
 */
class CameraAsyncTask private constructor(private var cameraCollector: CameraCollector) : AsyncTask<Void, Void, Void>() {

    companion object {
        /**
         * Interval time for each AsyncTask's execution
         */
        const val INTERVAL_TIME = 2000L
        const val DEBUG_TAG = "CameraAsyncTask"

        private var instance: CameraAsyncTask? = null

        fun stopInstance() {
            synchronized(this) {
                instance?.cancel(true)
                instance = null
            }
        }

        fun startInstance(cameraCollector: CameraCollector) {
            synchronized(this) {
                if (instance != null) {
                    stopInstance()
                }
                instance = CameraAsyncTask(cameraCollector)
                instance?.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
            }
        }
    }

    /**
     * Collect photo in the background and sleep specified interval time
     */
    override fun doInBackground(vararg params: Void?): Void? {
        Log.d(DEBUG_TAG,"background")
        cameraCollector.startCollector()
        Thread.sleep(INTERVAL_TIME)
        return null
    }

    /**
     * Start new instance of this class in order to start looping
     */
    override fun onPostExecute(result: Void?) {
        Log.d(DEBUG_TAG,"onPost")
        synchronized(this) {
            instance = CameraAsyncTask(cameraCollector)
            instance?.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
        }
        super.onPostExecute(result)
    }

    /**
     * Handle cancelling of this AsyncTask
     */
    override fun onCancelled() {
        Log.d(DEBUG_TAG,"onCancel")
        this.cancel(true)
        super.onCancelled()
    }
}