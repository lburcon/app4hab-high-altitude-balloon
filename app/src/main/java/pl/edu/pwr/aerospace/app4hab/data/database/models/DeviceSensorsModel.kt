package pl.edu.pwr.aerospace.app4hab.data.database.models

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Device sensors data class model
 */
@Entity(tableName = "device_data")
data class DeviceSensorsModel(
        @ColumnInfo(name = "time") var time: Long = System.currentTimeMillis(),
        @ColumnInfo(name = "pressure") var pressure: Float? = null,
        @ColumnInfo(name = "gravityX") var gravityX: Float? = null,
        @ColumnInfo(name = "gravityY") var gravityY: Float? = null,
        @ColumnInfo(name = "gravityZ") var gravityZ: Float? = null,
        @ColumnInfo(name = "rotationX") var rotationX: Double? = null,
        @ColumnInfo(name = "rotationY") var rotationY: Double? = null,
        @ColumnInfo(name = "rotationZ") var rotationZ: Double? = null,
        @ColumnInfo(name = "accelerationX") var accelerationX: Float? = null,
        @ColumnInfo(name = "accelerationY") var accelerationY: Float? = null,
        @ColumnInfo(name = "accelerationZ") var accelerationZ: Float? = null,
        @ColumnInfo(name = "magneticFieldX") var magneticFieldX: Float? = null,
        @ColumnInfo(name = "magneticFieldY") var magneticFieldY: Float? = null,
        @ColumnInfo(name = "magneticFieldZ") var magneticFieldZ: Float? = null,
        @ColumnInfo(name = "batteryPercentage") var batteryPercentage: Int? = null,
        @ColumnInfo(name = "batteryTemperature") var batteryTemperature: Float? = null,
        @ColumnInfo(name = "memoryUsage") var memoryUsage: Long? = null) {
    @ColumnInfo(name = "id")
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
}