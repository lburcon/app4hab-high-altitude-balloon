@file:Suppress("DEPRECATION")

package pl.edu.pwr.aerospace.app4hab.data.camera

import android.graphics.ImageFormat
import android.hardware.Camera
import android.util.Log


/**
 * Basic implementation of [CameraConfigurator]. It's prepared for Samsung Galaxy S8.
 */
class CameraConfiguratorImpl : CameraConfigurator {
    companion object {
        private const val DEBUG_TAG = "CameraConfigurator"
    }

    override fun configureCamera(params: Camera.Parameters): Camera.Parameters {
        val parameters: Camera.Parameters = params

        val previewSizes = parameters.supportedPreviewSizes
        val imageSizes = parameters.supportedPictureSizes

        parameters.setPreviewSize(previewSizes[0].width, previewSizes[0].height)
        parameters.setPictureSize(imageSizes[0].width, imageSizes[0].height)
        parameters.pictureFormat = ImageFormat.JPEG
        parameters.autoWhiteBalanceLock = false
        parameters.autoExposureLock = false
        parameters.focusMode = android.hardware.Camera.Parameters.FOCUS_MODE_AUTO
        //It needs to be auto in order to let focus callback work
        parameters.set("iso-values", "auto") //TODO Check if this value works
        //This will help us in taking pictures in sharp light
        parameters.set("burst-capture", 1)
        //I read in some article that this helps to stabilize
        //photos we could also try modify white-balance value to for example daylight

        println(parameters.flatten())
        //flatten is used to present parameters in a line, we can look into it
        //and see what else we want to add/change
        Log.d(DEBUG_TAG,"Configuration set.")
        return parameters
    }
}