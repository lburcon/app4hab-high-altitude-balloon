package pl.edu.pwr.aerospace.app4hab.data.camera

/**
 * Interface for saving photo.
 */
interface PhotoSaver {
    fun savePhoto(dataSet: ByteArray): Boolean
}