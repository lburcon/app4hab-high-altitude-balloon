package pl.edu.pwr.aerospace.app4hab.di.modules

import android.hardware.Camera
import android.os.Environment
import dagger.Module
import dagger.Provides
import pl.edu.pwr.aerospace.app4hab.data.camera.*
import java.io.File
import javax.inject.Named

/**
 * Created by t-rex on 14/03/2018.
 */
@Module(includes = arrayOf(FileModule::class))
class CameraCollectorModule {

    @Provides
    fun provideCamera(): Camera {
        return when (Camera.getNumberOfCameras()) {
            2 -> Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK)
            1 -> Camera.open()
            else -> throw IllegalStateException("Camera couldn't be initialized!")
        }
    }

    @Provides
    fun provideCameraConfigurator(): CameraConfigurator {
        return CameraConfiguratorImpl()
    }

    @Provides
    fun providePhotoSaver(@Named("cameraDirectory") mediaStorageDir: File): PhotoSaver {
        return PhotoSaverImpl(mediaStorageDir)
    }

    @Provides
    fun provideCameraCollector(camera: Camera,
                               cameraConfigurator: CameraConfigurator,
                               photoSaver: PhotoSaver): CameraCollector {
        return CameraCollectorImpl(camera, cameraConfigurator, photoSaver)
    }
}