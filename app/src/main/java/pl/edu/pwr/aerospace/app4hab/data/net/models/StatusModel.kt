package pl.edu.pwr.aerospace.app4hab.data.net.models
import com.google.gson.annotations.SerializedName

/**
 * Base status model
 *
 *
 * @param lat latitude in degrees
 * @param lon longitude in degrees
 * @param alt altitude in meters
 * @param outTemp outdoor temperature in celsius
 * @param inTemp indoor temperature in celsius
 * @param pressure atmospheric pressure in hPa
 * @param batteryVoltage battery voltage in V
 * @param atmegaReadID ID of last atmega message
 * @param lastSensorReading the time of last sensor reading
 * @param phoneTemp phone temperature in celsius
 */
data class StatusModel(@SerializedName("lat") val lat: Float? = null,
                       @SerializedName("lon") val lon: Float? = null,
                       @SerializedName("alt") val alt: Int? = null,
                       @SerializedName("outTemp") val outTemp: Float? = null,
                       @SerializedName("inTemp") val inTemp: Float? = null,
                       @SerializedName("pressure") val pressure: Float? = null,
                       @SerializedName("batteryVoltage") val batteryVoltage: Float? = null,
                       @SerializedName("atmegaReadID") val atmegaReadID: Int? = null,
                       @SerializedName("lastSensorReading") val lastSensorReading: Long? = null,
                       @SerializedName("phoneTemp") val phoneTemp: Float? = null) : App4HabHeadquartersResponseModel()