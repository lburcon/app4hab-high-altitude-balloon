package pl.edu.pwr.aerospace.app4hab.data.net.statusHandler

import android.util.Log
import pl.edu.pwr.aerospace.app4hab.data.database.dao.AtmegaDao
import pl.edu.pwr.aerospace.app4hab.data.database.dao.DeviceDao
import pl.edu.pwr.aerospace.app4hab.data.net.App4HabHeadquartersCallback
import pl.edu.pwr.aerospace.app4hab.data.net.App4HabHeadquartersService
import pl.edu.pwr.aerospace.app4hab.data.net.models.App4HabHeadquartersResponseModel
import pl.edu.pwr.aerospace.app4hab.data.net.models.App4HabHeadquartesException
import pl.edu.pwr.aerospace.app4hab.data.net.models.StatusModel

class StatusHandlerImpl(private val mAtmegaDao: AtmegaDao,
                        private val mDeviceDao: DeviceDao,
                        private val mHeadquartersService: App4HabHeadquartersService): StatusHandler {
    companion object {
        private const val DEBUG_TAG = "StatusHandlerImpl"
    }

    override fun sendStatus() {

        // Get last atmega model
        val lastAtmegaMeasurementList = mAtmegaDao.getLastMeasurement()
        val lastAtmegaMeasurement =if (lastAtmegaMeasurementList.isEmpty()) {
            null
        } else {
            lastAtmegaMeasurementList[0]
        }

        // Get last device model
        val lastDeviceMeasurementList = mDeviceDao.getLastMeasurement()
        val lastDeviceMeasurement =if (lastDeviceMeasurementList.isEmpty()) {
            null
        } else {
            lastDeviceMeasurementList[0]
        }

        val status = StatusModel(
                lastAtmegaMeasurement?.latitude,
                lastAtmegaMeasurement?.longitude,
                lastAtmegaMeasurement?.altitude,
                lastAtmegaMeasurement?.out_temp,
                lastAtmegaMeasurement?.inTemp,
                lastAtmegaMeasurement?.pressure,
                lastAtmegaMeasurement?.batteryVoltage,
                lastAtmegaMeasurement?.id?.toInt(),
                lastAtmegaMeasurement?.absoluteTime,
                lastDeviceMeasurement?.batteryTemperature

        )

        mHeadquartersService.sendStatus(status, object : App4HabHeadquartersCallback<App4HabHeadquartersResponseModel> {
            override fun onSuccess(responseModel: App4HabHeadquartersResponseModel) {
                Log.d(DEBUG_TAG,"Status ${status.atmegaReadID} send")
            }

            override fun onFail(reason: App4HabHeadquartesException) {
                Log.d(DEBUG_TAG,"Status ${status.atmegaReadID} failed")
            }
        })
    }
}