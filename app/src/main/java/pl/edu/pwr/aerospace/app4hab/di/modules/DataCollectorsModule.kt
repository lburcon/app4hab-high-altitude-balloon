package pl.edu.pwr.aerospace.app4hab.di.modules

import android.content.Context
import dagger.Module
import dagger.Provides
import pl.edu.pwr.aerospace.app4hab.data.collectors.device.*
import javax.inject.Named

/**
 * Created by t-rex on 18/03/2018.
 */
@Module(includes = arrayOf(ApplicationModule::class))
class DataCollectorsModule {

    @Provides
    @Named("acceleration")
    fun provideAccelerationDataCollector(context: Context): DataCollector {
        return AccelerationDataCollector(context)
    }

    @Provides
    @Named("battery")
    fun provideBatteryDataCollector(context: Context): DataCollector {
        return BatteryDataCollector(context)
    }

    @Provides
    @Named("gravity")
    fun provideGravityDataCollector(context: Context): DataCollector {
        return GravityDataCollector(context)
    }

    @Provides
    @Named("magnetic")
    fun provideMagneticDataCollector(context: Context): DataCollector {
        return MagneticDataCollector(context)
    }

    @Provides
    @Named("memory")
    fun provideMemoryUsageCollector(context: Context): DataCollector {
        return MemoryUsageCollector(context)
    }

    @Provides
    @Named("pressure")
    fun providePressureDataCollector(context: Context): DataCollector {
        return PressureDataCollector(context)
    }

    @Provides
    @Named("rotation")
    fun provideRotationDataCollector(context: Context): DataCollector {
        return RotationDataCollector(context)
    }

    @Provides
    fun provideDeviceSensorsDataCollector(
            @Named("acceleration") accCollector: DataCollector,
            @Named("battery") batteryCollector: DataCollector,
            @Named("gravity") gravityCollector: DataCollector,
            @Named("magnetic") magneticFieldCollector: DataCollector,
            @Named("memory") memoryDataCollector: DataCollector,
            @Named("pressure") pressureCollector: DataCollector,
            @Named("rotation") rotationCollector: DataCollector

    ): DeviceSensorsDataCollector {
        return DeviceSensorsDataCollectorImpl(
                accCollector,
                batteryCollector,
                gravityCollector,
                magneticFieldCollector,
                memoryDataCollector,
                pressureCollector,
                rotationCollector)
    }
}