package pl.edu.pwr.aerospace.app4hab.data.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import pl.edu.pwr.aerospace.app4hab.data.database.dao.AtmegaDao
import pl.edu.pwr.aerospace.app4hab.data.database.dao.DeviceDao
import pl.edu.pwr.aerospace.app4hab.data.database.models.AtmegaModel
import pl.edu.pwr.aerospace.app4hab.data.database.models.DeviceSensorsModel

/**
 * Main entity used for obtaining Application's database
 *
 */
@Database(entities = arrayOf(AtmegaModel::class, DeviceSensorsModel::class), version = 2, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun atmegaDao(): AtmegaDao
    abstract fun deviceDao(): DeviceDao
}