package pl.edu.pwr.aerospace.app4hab.data.collectors.device

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import pl.edu.pwr.aerospace.app4hab.data.database.models.DeviceSensorsModel

/**
 * Class responsible for collecting pressure data
 * Class implements [SensorEventListener] and [DataCollector] interface
 * @param context main application context
 */
class PressureDataCollector(context: Context) : SensorEventListener, DataCollector {

    private var pressure: Float? = null
    private val sensorManager: SensorManager by lazy {
        context.getSystemService(Context.SENSOR_SERVICE) as SensorManager
    }

    override fun onAccuracyChanged(p0: Sensor?, p1: Int) {
    }

    override fun onSensorChanged(event: SensorEvent?) {
        pressure = event?.values?.get(0)
    }

    override fun registerCollector() {
        sensorManager.registerListener(
                this,
                sensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE),
                SensorManager.SENSOR_DELAY_NORMAL
        )
    }

    /**
     * Writes pressure data to main phone sensors model
     */
    override fun getData(model: DeviceSensorsModel) {
        model.pressure = pressure
    }

    override fun unregisterCollector() {
        sensorManager.unregisterListener(this)
    }
}