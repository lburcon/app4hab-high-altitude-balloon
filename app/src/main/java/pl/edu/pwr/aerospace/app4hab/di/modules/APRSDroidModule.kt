package pl.edu.pwr.aerospace.app4hab.di.modules

import android.content.Context
import dagger.Module
import dagger.Provides
import pl.edu.pwr.aerospace.app4hab.data.aprsdroid.APRSDroidController
import pl.edu.pwr.aerospace.app4hab.data.aprsdroid.APRSDroidControllerImpl
import pl.edu.pwr.aerospace.app4hab.data.aprsdroid.APRSDroidEventReceiver

/**
 * Created by lukasz.burcon on 21.03.2018.
 */
@Module(includes = arrayOf(ApplicationModule::class))
class APRSDroidModule {

    @Provides
    fun provideAPRSDroidController(context: Context): APRSDroidController = APRSDroidControllerImpl(context)

    @Provides
    fun provideAPRSDroidEventReceiver(): APRSDroidEventReceiver = APRSDroidEventReceiver()
}