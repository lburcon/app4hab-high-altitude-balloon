package pl.edu.pwr.aerospace.app4hab.data.atmega

import android.content.Context
import android.hardware.usb.UsbDevice
import android.hardware.usb.UsbDeviceConnection
import android.hardware.usb.UsbManager
import android.os.AsyncTask
import android.util.Log
import com.felhr.usbserial.UsbSerialDevice
import com.felhr.usbserial.UsbSerialInterface
import pl.edu.pwr.aerospace.app4hab.data.DataController
import pl.edu.pwr.aerospace.app4hab.data.aprsdroid.APRSDroidControllerImpl
import pl.edu.pwr.aerospace.app4hab.data.threads.AtmegaAsyncTask


/**
 * Reads data from atmega
 */

class UsbReader(private val context: Context,
                private val dataController: DataController) {

    companion object {
        const val BAUD_RATE = 115200
        const val DEBUG_TAG = "UsbReader"
    }

    private val usbManger by lazy { context.getSystemService(Context.USB_SERVICE) as UsbManager }
    private val usbDevices by lazy { usbManger.deviceList }
    private lateinit var serialPort: UsbSerialDevice
    private var message = ""

    /**
     * Register device connected with USB to phone
     */
    fun registerUsbDownload() {
        Log.d(DEBUG_TAG,"start registerUsbDownload()")

        // assume 1 device on list
        if (usbDevices.size > 0) {
            val device = usbDevices.values.iterator().next()
            val connection = usbManger.openDevice(device)

            val callBack = UsbSerialInterface.UsbReadCallback { data: ByteArray ->
                if (data.isNotEmpty()){
                    message += String(data)
                    if (message.last() == '\n'){
                        AtmegaAsyncTask(dataController, message.toByteArray()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
                        message = ""
                    }
                }
            }

            if (connection != null) {
                createSerial(callBack, device, connection)
            }
        } else {
            Log.w(DEBUG_TAG,"There are no usb devices found")
        }
    }

    private fun createSerial(callBack: UsbSerialInterface.UsbReadCallback, device: UsbDevice, connection: UsbDeviceConnection?) {
        serialPort = UsbSerialDevice.createUsbSerialDevice(device, connection)

        if (serialPort.open()) {
            serialPort.apply {
                setBaudRate(BAUD_RATE)
                setDataBits(UsbSerialInterface.DATA_BITS_8)
                setStopBits(UsbSerialInterface.STOP_BITS_1)
                setParity(UsbSerialInterface.PARITY_NONE)
                read(callBack)
            }
        } else {
            Log.d(DEBUG_TAG,"serial port could not be opened")
        }
    }

    /**
     * Unregister USB device from phone
     */
    fun stopSerial() {
        serialPort.close()
        Log.d(DEBUG_TAG,"serial port has (hopefully) been closed")
    }
}