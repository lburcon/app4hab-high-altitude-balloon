package pl.edu.pwr.aerospace.app4hab.workers

import android.util.Log
import pl.edu.pwr.aerospace.app4hab.data.atmega.UsbReader


/**
 * Worker for Atmega
 * @param usbReader class with implementation for usb reading
 */
@WorkerService(WorkersNames.AtmegaWorker)
class AtmegaWorker(private val usbReader: UsbReader) : Worker {
    companion object {
        private val TAG = "AtmegaWorker"
    }

    @Volatile private var mState = WorkerState.STOPPED

    override fun stopWorker(): Boolean {
        return runStandardStoppingFlow { usbReader.stopSerial() }
    }

    override fun startWorker(): Boolean {
        return runStandardStartingFlow { usbReader.registerUsbDownload() }
    }

    override fun getState(): WorkerState {
        return mState
    }

    private fun runStandardStartingFlow(startingActions: () -> Unit): Boolean {
        try {
            Log.d(TAG,"Service is starting")
            return if (mState == WorkerState.STOPPED) {
                mState = WorkerState.LAUNCHING
                startingActions.invoke()
                Log.d(TAG,"Service is started")
                mState = WorkerState.RUNNING
                true
            } else {
                Log.d(TAG,"Service starting failed.")
                false
            }
        } catch (ex: Exception) {
            Log.d(TAG,"Sth gone wrong.")
            mState = WorkerState.BROKEN
            return false
        }
    }

    private fun runStandardStoppingFlow(stoppingActions: () -> Unit): Boolean {
        try {
            Log.d(TAG,"Service is stopping")
            return if (mState == WorkerState.RUNNING) {
                mState = WorkerState.STOPPING
                stoppingActions.invoke()
                Log.d(TAG,"Service is stopped")
                mState = WorkerState.STOPPED
                true
            } else {
                Log.d(TAG,"Service stopping failed.")
                false
            }
        } catch (ex: Exception) {
            Log.d(TAG,"Sth gone wrong.")
            mState = WorkerState.BROKEN
            return false
        }
    }
}