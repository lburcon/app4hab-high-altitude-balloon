package pl.edu.pwr.aerospace.app4hab.data.net.photoHandler

import android.content.Context
import android.util.Base64
import android.util.Log
import id.zelory.compressor.Compressor
import pl.edu.pwr.aerospace.app4hab.data.database.dao.AtmegaDao
import pl.edu.pwr.aerospace.app4hab.data.net.App4HabHeadquartersCallback
import pl.edu.pwr.aerospace.app4hab.data.net.App4HabHeadquartersService
import pl.edu.pwr.aerospace.app4hab.data.net.models.App4HabHeadquartersResponseModel
import pl.edu.pwr.aerospace.app4hab.data.net.models.App4HabHeadquartesException
import pl.edu.pwr.aerospace.app4hab.data.net.models.PhotoModel

import java.io.File

/**
 * Used for sending photos to server
 */
class PhotoHandlerImpl(private val mContext: Context,
                       private val mPhotoDirectory: File,
                       private val mAtmegaDao: AtmegaDao,
                       private val mHeadquartersService: App4HabHeadquartersService) : PhotoHandler {

    companion object {
        private const val DEBUG_TAG = "PhotoHandler"

        private const val MAX_HEIGHT = 640
        private const val MAX_WIDTH = 480
        private const val QUALITY = 75
    }

    private fun compress(photoFile: File): File {
        return Compressor(mContext)
                .setMaxHeight(MAX_HEIGHT)
                .setMaxWidth(MAX_WIDTH)
                .setQuality(QUALITY)
                .compressToFile(photoFile)
    }

    override fun sendLastPhoto() {
        if (!mPhotoDirectory.canRead()) {
            Log.d(DEBUG_TAG,"Directory is not readable")
            return
        }

        val sortedPhotos = mPhotoDirectory
                .listFiles()
                .filter {
                    it.extension == "jpg"
                }.sortedBy { x -> x.lastModified() }

        if (sortedPhotos.isEmpty()) {
            Log.d(DEBUG_TAG,"Folder doesn't contains any files!")
            return
        }
        val lastPhoto = sortedPhotos.last()

        if (!lastPhoto.canRead()) {
            Log.d(DEBUG_TAG,"Opened file ${lastPhoto.path} is not readable")
        }

        val lastAtmegaMeasurementList = mAtmegaDao.getLastMeasurement()
        val lastAtmegaMeasurement =if(lastAtmegaMeasurementList.isEmpty()) {
            null
        } else {
            lastAtmegaMeasurementList[0]
        }

        val photoString = compress(lastPhoto).let {
            Base64.encodeToString(it.readBytes(), Base64.DEFAULT)
        }

        val photoModel = PhotoModel(
                photoString,
                lastPhoto.lastModified(),
                sortedPhotos.count(),
                lastAtmegaMeasurement?.longitude,
                lastAtmegaMeasurement?.latitude,
                lastAtmegaMeasurement?.altitude
        )

        mHeadquartersService.sendPhoto(
                photoModel,
                object : App4HabHeadquartersCallback<App4HabHeadquartersResponseModel> {
                    override fun onSuccess(responseModel: App4HabHeadquartersResponseModel) {
                        Log.d(DEBUG_TAG,"Sending file ${lastPhoto.path} finished with success")
                    }

                    override fun onFail(reason: App4HabHeadquartesException) {
                        Log.d(DEBUG_TAG,"Sending file ${lastPhoto.path} finished with error ${reason.errorCode}")
                    }
                }
        )
    }

}