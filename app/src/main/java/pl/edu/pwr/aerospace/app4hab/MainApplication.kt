package pl.edu.pwr.aerospace.app4hab

import android.app.Application
import android.os.Environment
import com.facebook.stetho.Stetho
import com.google.gson.Gson
import pl.edu.pwr.aerospace.app4hab.di.components.ApplicationComponent
import pl.edu.pwr.aerospace.app4hab.di.components.DaggerApplicationComponent
import pl.edu.pwr.aerospace.app4hab.di.modules.ApplicationModule
import pl.edu.pwr.aerospace.app4hab.di.modules.NetModule
import java.io.File
import java.io.BufferedReader
import java.io.FileReader
import java.io.IOException


/**
 * Main Application class
 *
 */
class MainApplication : Application() {

    /**
     * Dagger component
     */
    private val mComponent by lazy {
        DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(this))
                .netModule(NetModule(readSettingsFromFile()))
                .build()
    }

    /**
     * Used for initializing external libraries
     */
    override fun onCreate() {
        super.onCreate()
        Stetho.initializeWithDefaults(this)
    }

    /**
     * Returns Dagger's component
     */
    fun getApplicationComponent(): ApplicationComponent = mComponent

    private fun readSettingsFromFile(): SettingsModel {
        val sdcard = Environment.getExternalStorageDirectory()
        val file = File(sdcard, "app4hab.json")
        val stringBuilder = StringBuilder()

        try {
            val br = BufferedReader(FileReader(file))
            br.forEachLine {
                stringBuilder.append(it)
            }

            br.close()
        } catch (e: IOException) {
        }

        return Gson().fromJson(stringBuilder.toString(), SettingsModel::class.java)
    }
}